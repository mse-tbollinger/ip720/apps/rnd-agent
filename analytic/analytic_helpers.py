from typing import List, Sequence, Tuple
from tensorflow.python.summary.summary_iterator import summary_iterator
import matplotlib.pyplot as plt
import pandas as pd
from collections import defaultdict
from utlis.io_helper import OsPath
from tensorflow.python.framework import tensor_util
import os
import yaml
import numpy as np
import tensorflow as tf

import seaborn as sns; sns.set()

from tensorflow.core.util import event_pb2
from tensorflow.python.lib.io import tf_record
from matplotlib.image import imread

level_img_left = -7.5
level_img_right = 37.2
level_img_bottom = -9.9
level_img_top = 8.1

def plot_level(img: np.ndarray, ax: plt.Axes = None) -> None:
    if ax is None:
        plt.imshow(img, zorder=0, extent=[level_img_left, level_img_right, level_img_bottom, level_img_top])
    else:
        ax.imshow(img, zorder=0, extent=[level_img_left, level_img_right, level_img_bottom, level_img_top])

def disable_grid_and_axis(ax: plt.Axes = None) -> None:
    if ax is None:
        plt.grid(b=None)
        plt.axis('off')
    else:
        ax.grid(b=None)
        ax.axis('off')

def plot_position_slice(start: int, end: int, img: np.ndarray, df: pd.DataFrame) -> None:
    plt.figure(figsize=(20,10))
    df_slice = df.iloc[start:end]
    start_step = start
    end_step = end
    df_slice.plot.scatter('x', 'y', c='curiosity', s=5, colormap='cool', figsize=(20,7), title=f"Explored regions and the corresponding curiosity from step {start_step} to  {end_step}")
    plot_level(img)
    plt.grid(b=None)
    plt.axis('off')
    plt.show()

def create_hight_curiosity_plot(data_frame: pd.DataFrame, 
                                name: str, 
                                threshold: float, 
                                ax: plt.Axes = None, 
                                figsize: Tuple[int, int]=(20,7), 
                                size: float=1) -> plt.Axes:
    return data_frame.plot.scatter('x', 'y', c='curiosity', colormap='cool', 
                                    s=size, figsize=figsize, title=f"only curiosity values above {threshold} in {name}", ax=ax)

def annotate_bugs_plot(ax: plt.Axes, names: Sequence[str], xs: Sequence[float], ys: Sequence[float], char_size: float = 0.3) -> None:
    x_text = xs[0] - 5
    for txt, x, y in zip(names, xs, ys):
        ax.annotate(txt, (x, y), xytext=[x_text, 7], color='white', arrowprops={'arrowstyle': '-', 'color': 'blue', 'lw': 2})
        x_text += len(txt)*char_size

def create_bugs_in_level_plot(data_frame: pd.DataFrame, name: str, ax: plt.Axes = None, char_size: float = 0.3, figsize: Tuple[int, int] = (20,7)) -> plt.Axes:
    ax = data_frame.plot.scatter('X', 'Y', s=data_frame.Radius*800, figsize=figsize, title=f"Bugs in {name}", color='none', edgecolors='r', ax=ax)
    df_bugs_plot = data_frame.sort_values('X').reset_index()
    annotate_bugs_plot(ax, df_bugs_plot.Name, df_bugs_plot.X, df_bugs_plot.Y, char_size)
    return ax