from typing import Any, Dict, List
from tensorflow.python.summary.summary_iterator import summary_iterator
from collections import defaultdict
from utlis.io_helper import OsPath
from tensorflow.python.framework import tensor_util
import os
import yaml

from tensorflow.core.util import event_pb2
from tensorflow.python.lib.io import tf_record

def parse_tensorboard_file(event_folder: str) -> Dict[str, Any]:
    dict = defaultdict(list)

    def my_summary_iterator(path: str) -> Any:
        for r in tf_record.tf_record_iterator(path):
            yield event_pb2.Event.FromString(r)

    for filename in os.listdir(str(event_folder)):
        path = os.path.join(str(event_folder), filename)
        for event in my_summary_iterator(path):
            if len(event.summary.value) > 0:
                dict['step'].append(event.step)
            for value in event.summary.value:
                t = tensor_util.MakeNdarray(value.tensor)
                try:
                    if t.size == 1:
                        dict[value.tag].append(t.item(0))
                    else:
                        dict[value.tag].append(t)
                except Exception as e:
                    print(type(t))

    return dict


def save(path: OsPath, event_file: OsPath) -> Dict[str, Any]:
    tf_data = parse_tensorboard_file(str(event_file))
    data = {
        # 'step': tf_data['step'],
        'x': tf_data['eval/step/detailed/x'],
        'y': tf_data['eval/step/detailed/y'],
        'ix': tf_data['eval/step/detailed/ix'],
        'iy': tf_data['eval/step/detailed/iy'],
        'curiosity': tf_data['eval/step/detailed/curiosity']}
    yaml.dump(data, path.open_write(), Dumper=yaml.dumper.SafeDumper)

    return tf_data

def save_eval(path: OsPath, event_file: OsPath) -> Dict[str, Any]:
    tf_data = parse_tensorboard_file(str(event_file))
    data = {
        # 'step': tf_data['step'],
        'x': tf_data['eval/step/detailed/x'],
        'y': tf_data['eval/step/detailed/y'],
        'ix': tf_data['eval/step/detailed/ix'],
        'iy': tf_data['eval/step/detailed/iy'],
        'curiosity': tf_data['eval/step/detailed/curiosity']}
    yaml.dump(data, path.open_write(), Dumper=yaml.dumper.SafeDumper)

    return tf_data

def save_train(path: OsPath, event_file: OsPath) -> Dict[str, Any]:
    tf_data = parse_tensorboard_file(str(event_file))
    data = {
        # 'step': tf_data['step'],
        'action_0': tf_data['driver_loop/step/detailed/action_0'],
        'action_1': tf_data['driver_loop/step/detailed/action_1'],
        'curiosity': tf_data['driver_loop/step/detailed/curiosity'],
        'extrinsic_reward': tf_data['driver_loop/step/detailed/extrinsic reward'],
        'x': tf_data['driver_loop/step/detailed/x'],
        'y': tf_data['driver_loop/step/detailed/y'],
        'ix': tf_data['driver_loop/step/detailed/ix'],
        'iy': tf_data['driver_loop/step/detailed/iy'],
        'total_reward': tf_data['driver_loop/step/detailed/total reward'],
        # 'explored': tf_data['driver_loop/step/explored'],
        # 'extrinsic_reward_mean': tf_data['driver_loop/step/extrinsic reward mean'],
        # 'total_reward_mean': tf_data['driver_loop/step/total reward mean'],
        # 'curiosity_mean': tf_data['driver_loop/step/curiosity mean']
    }
    yaml.dump(data, path.open_write(), Dumper=yaml.dumper.SafeDumper)

    return tf_data

def save_icm(path: OsPath, event_file: OsPath) -> Dict[str, Any]:
    tf_data = parse_tensorboard_file(str(event_file))
    data = {
        # 'step': tf_data['step'],
        'forward_loss_mean': tf_data['icm/forward_loss_mean'],
        'inverse_loss_mean': tf_data['cm/inverse_loss_mean'],
        'mean_loss': tf_data['icm/mean loss'],
        'std': tf_data['icm/std']}
    yaml.dump(data, path.open_write(), Dumper=yaml.dumper.SafeDumper)

    return tf_data


def load(path: OsPath) -> Dict[str, Any]:
    return yaml.load(path.open_read(), Loader=yaml.loader.SafeLoader)