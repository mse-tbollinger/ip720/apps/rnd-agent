from utlis.io_helper import OsPath
from runners.rnd_train_runner import RndTrainRunner
from Networks.reward_preprocessor import RewardPreprocessorBase
from Networks.obervation_preprocessors import ObservationPreprocessorBase
from typing import Sequence, Tuple
from Networks.reward_preprocessor.rdn_reward_preprocessor import RndRewardPreprocessor
from Networks.obervation_preprocessors.cnn_obs_preprocessor import CnnObservationsPreprocessor


class RndTestRunner(RndTrainRunner):

    def __init__(self, config_path: OsPath) -> None:
        super().__init__(config_path)

    def network_factory(self, shape: Sequence[int]) -> Tuple[ObservationPreprocessorBase, RewardPreprocessorBase]:
        obs_network = CnnObservationsPreprocessor(shape, layers=self.config.cnn_obs_preprocessor.layers)
        reward_network = RndRewardPreprocessor(
            obs_network,
            "rnd_reward_network",
            dense_specs_predictor=self.config.rnd_reward_preprocessor.dense_spec_predictor,
            dense_specs_target=self.config.rnd_reward_preprocessor.dense_spec_target,
            curiosity_normalizer=self.config.rnd_reward_preprocessor.curiosity_normalizer)

        reward_network.freeze()

        return obs_network, reward_network
