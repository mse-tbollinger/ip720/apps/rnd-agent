from typing import Sequence, Tuple, Optional

from tf_agents.environments.tf_environment import TFEnvironment
from tf_agents.networks.actor_distribution_network import ActorDistributionNetwork
from tf_agents.networks.network import DistributionNetwork, Network
from tf_agents.networks.value_network import ValueNetwork

from Networks.obervation_preprocessors import ObservationPreprocessorBase
from Networks.obervation_preprocessors.cnn_obs_preprocessor import CnnObservationsPreprocessor
from Networks.reward_preprocessor import RewardPreprocessorBase
from Networks.reward_preprocessor.intrinsic_curiosity_model import IntrinsicCuriosityModel
from runners import BaseRunner
from utlis.config_helper import NetConfig
import tensorflow as tf


class IcmTrainRunner(BaseRunner):
    def network_factory(self, shape: Sequence[int]) -> Tuple[ObservationPreprocessorBase, RewardPreprocessorBase]:
        obs_network = CnnObservationsPreprocessor(shape, layers=self.config.cnn_obs_preprocessor.layers)
        reward_network = IntrinsicCuriosityModel(
            obs_network,
            action_len=2,
            name="rnd_reward_network",
            dense_specs_forward_model=self.config.rnd_reward_preprocessor.dense_spec_predictor,
            dense_specs_invers_model=self.config.rnd_reward_preprocessor.dense_spec_target,
            curiosity_normalizer=self.config.rnd_reward_preprocessor.curiosity_normalizer)

        return obs_network, reward_network

    def get_actor_distribution_net(self, train_env: TFEnvironment, config: NetConfig, preprocessor_model: Optional[tf.keras.Model]) -> DistributionNetwork:
        network = ActorDistributionNetwork(
            preprocessing_layers=preprocessor_model,
            input_tensor_spec=train_env.observation_spec(),
            output_tensor_spec=train_env.action_spec(),
            fc_layer_params=config.fc_layer_params,
            conv_layer_params=config.conv_layer_params,
            activation_fn=config.activation_fn,
            name="actor_distribution_net"
        )

        return network

    def get_value_network(self, train_env: TFEnvironment, config: NetConfig, preprocessor_model: Optional[tf.keras.Model]) -> Network:
        network = ValueNetwork(
            preprocessing_layers=preprocessor_model,
            input_tensor_spec=train_env.observation_spec(),
            fc_layer_params=config.fc_layer_params,
            conv_layer_params=config.conv_layer_params,
            activation_fn=config.activation_fn,
            name="value_network"
        )

        return network