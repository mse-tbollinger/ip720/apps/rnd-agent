from Networks.obervation_preprocessors import ObservationPreprocessorBase
from Networks.reward_preprocessor import RewardPreprocessorBase
from runners.icm_train_runner import IcmTrainRunner

from typing import Sequence, Tuple


class RndRnnEvalRunner(IcmTrainRunner):

    def network_factory(self, shape: Sequence[int]) -> Tuple[ObservationPreprocessorBase, RewardPreprocessorBase]:
        obs_network, reward_network = super(RndRnnEvalRunner, self).network_factory(shape)
        reward_network.freeze()
        return obs_network, reward_network
