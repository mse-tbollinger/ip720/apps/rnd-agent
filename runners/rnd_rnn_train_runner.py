from tf_agents.networks import normal_projection_network
from Networks.reward_preprocessor.rdn_reward_preprocessor import RndRewardPreprocessor
from Networks.obervation_preprocessors.cnn_obs_preprocessor import CnnObservationsPreprocessor
from tf_agents.environments.tf_environment import TFEnvironment
from utlis.config_helper import NetConfig
from tf_agents.networks.network import DistributionNetwork, Network
from Networks.reward_preprocessor import RewardPreprocessorBase
from Networks.obervation_preprocessors import ObservationPreprocessorBase
from typing import Sequence, Tuple, Optional
from runners import BaseRunner
from tf_agents.networks.value_rnn_network import ValueRnnNetwork
from tf_agents.networks.actor_distribution_rnn_network import ActorDistributionRnnNetwork
import tensorflow as tf
import numpy as np


def _normal_projection_net(action_spec,
                           init_action_stddev=0.35,
                           init_means_output_factor=0.6):
    std_bias_initializer_value = np.log(np.exp(init_action_stddev) - 1)

    return normal_projection_network.NormalProjectionNetwork(
      action_spec,
      init_means_output_factor=init_means_output_factor,
      std_bias_initializer_value=std_bias_initializer_value)

class RndRnnTrainRunner(BaseRunner):
    def network_factory(self, shape: Sequence[int]) -> Tuple[ObservationPreprocessorBase, RewardPreprocessorBase]:
        obs_network = CnnObservationsPreprocessor(shape, layers=self.config.cnn_obs_preprocessor.layers)
        reward_network = RndRewardPreprocessor(
            obs_network,
            "rnd_reward_network",
            dense_specs_predictor=self.config.rnd_reward_preprocessor.dense_spec_predictor,
            dense_specs_target=self.config.rnd_reward_preprocessor.dense_spec_target,
            curiosity_normalizer=self.config.rnd_reward_preprocessor.curiosity_normalizer)

        return obs_network, reward_network

    def get_actor_distribution_net(self, train_env: TFEnvironment, config: NetConfig, preprocessor_model: Optional[tf.keras.Model]) -> DistributionNetwork:
        network = ActorDistributionRnnNetwork(
            input_fc_layer_params=(256,),
            lstm_size=(256,),
            output_fc_layer_params=(128,),
            preprocessing_layers=preprocessor_model,
            input_tensor_spec=train_env.observation_spec(),
            output_tensor_spec=train_env.action_spec(),
            conv_layer_params=config.conv_layer_params,
            activation_fn=config.activation_fn,
            name="actor_distribution_net",
            continuous_projection_net=_normal_projection_net)

        return network

    def get_value_network(self, train_env: TFEnvironment, config: NetConfig, preprocessor_model: Optional[tf.keras.Model]) -> Network:
        network = ValueRnnNetwork(
            preprocessing_layers=preprocessor_model,
            input_tensor_spec=train_env.observation_spec(),
            conv_layer_params=config.conv_layer_params,
            activation_fn=config.activation_fn,
            input_fc_layer_params=(256,),
            lstm_size=(256,),
            output_fc_layer_params=(128,),
            name="value_network")

        return network
