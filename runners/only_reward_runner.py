from utlis.io_helper import OsPath
from Networks.reward_preprocessor.zero_reward_preprocessor import ZeroRewardPreprocessor
from Networks.obervation_preprocessors.passthrough_obs_preprocessor import PassthroughObservationsPreprocessor
from tf_agents.environments.tf_environment import TFEnvironment
from utlis.config_helper import NetConfig
from tf_agents.networks.network import DistributionNetwork, Network
from Networks.reward_preprocessor import RewardPreprocessorBase
from Networks.obervation_preprocessors import ObservationPreprocessorBase
from typing import Sequence, Tuple, Optional
from runners import BaseRunner
from tf_agents.networks.actor_distribution_rnn_network import ActorDistributionRnnNetwork
from tf_agents.networks.value_rnn_network import ValueRnnNetwork
import tensorflow as tf


class OnlyRewardRunner(BaseRunner):

    def __init__(self, config_path: OsPath) -> None:
        super(OnlyRewardRunner, self).__init__(config_path)
        if self.config.env.curiosity_factor != 0:
            raise ValueError("The OnlyRewardRunner can only be used with a Curiosity factor of 0")

    def network_factory(self, shape: Sequence[int]) -> Tuple[ObservationPreprocessorBase, RewardPreprocessorBase]:
        obs_network = PassthroughObservationsPreprocessor(shape)
        reward_network = ZeroRewardPreprocessor(
            obs_network,
            "rnd_reward_network")

        return obs_network, reward_network

    def get_actor_distribution_net(self, train_env: TFEnvironment, config: NetConfig, preprocessor_model: Optional[tf.keras.Model]) -> DistributionNetwork:
        network = ActorDistributionRnnNetwork(
            input_fc_layer_params=(256,),
            lstm_size=(256,),
            output_fc_layer_params=(128,),
            preprocessing_layers=preprocessor_model,
            input_tensor_spec=train_env.observation_spec(),
            output_tensor_spec=train_env.action_spec(),
            conv_layer_params=config.conv_layer_params,
            activation_fn=config.activation_fn,
            name="actor_distribution_net")

        return network

    def get_value_network(self, train_env: TFEnvironment, config: NetConfig, preprocessor_model: Optional[tf.keras.Model]) -> Network:
        network = ValueRnnNetwork(
            preprocessing_layers=preprocessor_model,
            input_tensor_spec=train_env.observation_spec(),
            conv_layer_params=config.conv_layer_params,
            activation_fn=config.activation_fn,
            input_fc_layer_params=(256,),
            lstm_size=(256,),
            output_fc_layer_params=(128,),
            name="value_network")

        return network