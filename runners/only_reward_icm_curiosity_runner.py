import time
import logging

import numpy as np
from tf_agents.trajectories import time_step as ts
from utlis.io_helper import OsPath
from tf_agents.environments.tf_environment import TFEnvironment
from utlis.config_helper import NetConfig
from tf_agents.networks.network import DistributionNetwork, Network
from Networks.reward_preprocessor import RewardPreprocessorBase
from Networks.obervation_preprocessors import ObservationPreprocessorBase
from typing import Any, Sequence, Tuple, Optional
from runners import BaseRunner
from tf_agents.networks.value_rnn_network import ValueRnnNetwork
from tf_agents.networks.actor_distribution_rnn_network import ActorDistributionRnnNetwork
import tensorflow as tf
from runners.icm_train_runner import IcmTrainRunner
from tf_agents.trajectories.time_step import TimeStep
from tf_agents.trajectories.policy_step import PolicyStep


class OnlyRewardCuriosityRunner(BaseRunner):

    def __init__(self, config_path: OsPath) -> None:
        self._config_path = config_path
        super().__init__(config_path)
        if(self.config.env.curiosity_factor != 0):
            raise ValueError("The OnlyRewardRunner can only be used with a Curiosity factor of 0")

    def network_factory(self, shape: Sequence[int]) -> Tuple[ObservationPreprocessorBase, RewardPreprocessorBase]:
        icm_runner = IcmTrainRunner(self._config_path)
        return icm_runner.network_factory(shape)

    def get_actor_distribution_net(self, train_env: TFEnvironment, config: NetConfig, preprocessor_model: Optional[tf.keras.Model]) -> DistributionNetwork:
        network = ActorDistributionRnnNetwork(
            input_fc_layer_params=(256,),
            lstm_size=(256,),
            output_fc_layer_params=(128,),
            preprocessing_layers=preprocessor_model,
            input_tensor_spec=train_env.observation_spec(),
            output_tensor_spec=train_env.action_spec(),
            conv_layer_params=config.conv_layer_params,
            activation_fn=config.activation_fn,
            name="actor_distribution_net")

        return network

    def get_value_network(self, train_env: TFEnvironment, config: NetConfig, preprocessor_model: Optional[tf.keras.Model]) -> Network:
        network = ValueRnnNetwork(
            preprocessing_layers=preprocessor_model,
            input_tensor_spec=train_env.observation_spec(),
            conv_layer_params=config.conv_layer_params,
            activation_fn=config.activation_fn,
            input_fc_layer_params=(256,),
            lstm_size=(256,),
            output_fc_layer_params=(128,),
            name="value_network")

        return network

    def save_checkpoint(self, ppo_step_val: np.ndarray, save_policy=True) -> None:
        self.train_env.save(self.saved_model_dir)
        self.ppo_vars.save(self.saved_model_dir)
        self.env_vars.save(self.saved_model_dir)

    def _env_steps(self, n_steps: int) -> None:
        time_step: TimeStep = self.eval_env.reset()
        policy_state = self.agent.policy.get_initial_state(self.eval_env.batch_size)
        # random_policy = RandomTFPolicy(self.train_env.time_step_spec(), self.train_env.action_spec())

        def policy_action(time_step: TimeStep, policy_state: Any) -> PolicyStep:
            # if random() < 0.8:
            return self.agent.policy.action(time_step, policy_state)
            # else:
            #     return random_policy.action(time_step, policy_state)
    
        for _ in range(n_steps):
            policy_step = policy_action(time_step, policy_state)
            time_step = self.eval_env.step(policy_step.action)
            policy_state = policy_step.state

    def train(self) -> None:
        print("begin training")

        collect_time = 0.0
        train_time = 0.0
        time_at_step = self.ppo_vars.steps.numpy()

        with self.ppo_vars.writer.as_default():
            iterations = 0
            while iterations < self.config.runner.num_iterations:
                ppo_step_val = self.ppo_vars.steps.numpy()
                env_step_val = self.env_vars.steps.numpy()
                start_time = time.time()

                self._env_steps(self.config.runner.collect_step_per_iteration)
                collect_time += time.time() - start_time
                start_time = time.time()
                train_time += time.time() - start_time
                self.ppo_vars.steps = tf.add(self.ppo_vars.steps, 1)

                if iterations % self.config.runner.train_checkpoint_interval == 0:
                    steps_per_sec = (
                            (env_step_val - time_at_step) / (collect_time + train_time))
                    logging.info('%.3f steps/sec', steps_per_sec)
                    logging.info('collect_time = %.3f, train_time = %.3f', collect_time,
                                train_time)
                                
                    if self.config.runner.save_model:
                        self.save_checkpoint(ppo_step_val)

                    time_at_step = ppo_step_val
                    collect_time = 0
                    train_time = 0

                if iterations % self.config.runner.eval_interval == 0:
                    self.train_env.reset()
                    self.eval(iterations)

                iterations += 1

        ppo_step_val = self.ppo_vars.steps.numpy()
        self.save_checkpoint(ppo_step_val)
        self.train_env.close()

    def run(self, n_steps: int) -> None:
        time_step: ts.TimeStep = self.train_env.reset()
        for _ in range(n_steps):
            action = self.agent.policy.action(time_step)
            time_step = self.train_env.step(action[0])