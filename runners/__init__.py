from tf_agents.policies.fixed_policy import FixedPolicy
from tf_agents.trajectories.trajectory import Trajectory

from enviroments.platformer_tf_eval_environment import PlatformerTfEvalEnvironment
import logging
import os
import time
from datetime import datetime
from typing import Tuple, Sequence, Optional
import numpy as np
import tf_agents

import tensorflow as tf
from tf_agents.agents import tf_agent
from tf_agents.agents.tf_agent import TFAgent
from tf_agents.environments.tf_environment import TFEnvironment
from tf_agents.networks.network import DistributionNetwork, Network
from tf_agents.replay_buffers.replay_buffer import ReplayBuffer
from tf_agents.trajectories import time_step as ts
from tf_agents.agents.ppo.ppo_clip_agent import PPOClipAgent
from tf_agents.drivers.dynamic_step_driver import DynamicStepDriver
from tf_agents.eval import metric_utils
from tf_agents.metrics import tf_metrics
from tf_agents.policies import policy_saver
from tf_agents.policies.random_tf_policy import RandomTFPolicy
from tf_agents.replay_buffers.tf_uniform_replay_buffer import TFUniformReplayBuffer
from tf_agents.trajectories.time_step import TimeStep
from tf_agents.utils import common

import reports.wand_helper
from Networks.obervation_preprocessors import ObservationPreprocessorBase
from Networks.reward_preprocessor import RewardPreprocessorBase
from enviroments.platformer_tf_environment import PlatformerTfEnvironment
from utlis.bug_reporter import BugReporter
from utlis.config_helper import NetConfig, PPOConfig, PlatformerConfig, RunnerConfig
from utlis.io_helper import OsPath
from utlis.runner_variables import EnvStepVars, PpoVars
import matplotlib.pyplot as plt


class BaseRunner:
    def __init__(self, config_path: OsPath) -> None:
        config = PlatformerConfig.from_file(config_path)
        model_dir = config.model_dir
        root_dir = config.runner.model_root_dir
        self.config = config

        if model_dir is None:
            model_dir = OsPath(f"{datetime.now().strftime('%Y-%m-%d-%H-%M')}/")
        self.model_dir = model_dir

        if config.runner.log_wandb:
            reports.wand_helper.init_wandb()
            reports.wand_helper.wandb.save(str(config_path))
            reports.wand_helper.wandb.config.update(config)
            reports.wand_helper.wandb.config.update({'model_dir': str(model_dir)}, allow_val_change=True)

        path = root_dir + model_dir
        train_dir = path + 'train/'
        env_dir = path + 'env'
        eval_env_dir = path + 'env_eval'
        self.saved_policy_model_dir = path + 'policy_saved_model/'
        self.saved_model_dir = path + 'saved_model/'

        # Tensorboard
        ppo_summary_writer = tf.summary.create_file_writer(
            str(train_dir), flush_millis=1000, name="ppo")
        self.eval_summary_writer = tf.summary.create_file_writer(
            str(path + 'eval/'), flush_millis=1000, name="eval")

        # profiler.experimental.start(str(path + 'profile'))

        self.ppo_vars = PpoVars(ppo_summary_writer)
        self.env_vars = EnvStepVars(env_dir)
        self.eval_env_vars = EnvStepVars(eval_env_dir)

        if self.saved_model_dir.exists():
            self.env_vars.load(self.saved_model_dir)
            self.ppo_vars.load(self.saved_model_dir)
            self.eval_env_vars.load(self.saved_model_dir)

        bug_locations_file = OsPath(config.eval_env.data_folder) + "bug_locations.json"
        self.bug_reporter = BugReporter(bug_locations_file)

        self.train_env = PlatformerTfEnvironment(config.env.env_name,
                                            OsPath(config.env.data_folder),
                                            self.network_factory,
                                            self.env_vars,
                                            bug_reporter=None,
                                            config=config.env,
                                            debug_step_log_interval=config.env.debug_step_log_interval,
                                            high_curiosity_threshold=config.env.hight_curiosity_threshold)

        self.eval_env = PlatformerTfEvalEnvironment(config.eval_env.env_name,
                                            OsPath(config.eval_env.data_folder),
                                            self.train_env.observation_preprocessor,
                                            self.train_env.reward_predictor,
                                            self.eval_env_vars,
                                            config=config.eval_env,
                                            bug_reporter=self.bug_reporter,
                                            debug_step_log_interval=config.eval_env.debug_step_log_interval,
                                            high_curiosity_threshold=config.eval_env.hight_curiosity_threshold)

        # self.train_env.reward_predictor.plot_model(path)
        # self.train_env.observation_preprocessor.plot_model(path)

        if self.saved_model_dir.exists() and config.runner.load_exploration:
            self.train_env.load_explorable_area(self.saved_model_dir)
            self.eval_env.load_explorable_area(self.saved_model_dir)

        if self.saved_model_dir.exists() and self.config.runner.load_reward_predictor:
            self.train_env.load_predictor_model(self.saved_model_dir)

        optimizer = tf.compat.v1.train.AdamOptimizer(learning_rate=config.ppo.learning_rate)

        preprocessor_model = self.get_preprocessor_model(config.ppo, self.train_env.observation_spec().shape)
        self.actor_distribution_net = self.get_actor_distribution_net(self.train_env, config.ppo.actor_distribution_net, preprocessor_model)
        self.value_network = self.get_value_network(self.train_env, config.ppo.value_network, preprocessor_model)
        self.agent = self.get_agent(self.train_env, optimizer, self.actor_distribution_net, self.value_network, config.ppo)
        self.agent.initialize()

        self.environment_steps_metric = tf_metrics.EnvironmentSteps()
        step_metrics = [
            tf_metrics.NumberOfEpisodes(),
            self.environment_steps_metric
        ]

        self.train_metrics = step_metrics + [
            tf_metrics.AverageReturnMetric(
                batch_size=config.env.batch_size),
            tf_metrics.AverageEpisodeLengthMetric(
                batch_size=config.env.batch_size),
        ]

        self.replay_buffer = self.get_replay_buffer(self.agent, self.train_env, config.runner.replay_buffer_max_length)

        if not self.saved_model_dir.exists():
            self.make_initial_steps()
        self.train_env.reset()

        self.train_checkpointer = common.Checkpointer(
            ckpt_dir=str(train_dir),
            max_to_keep=100,
            agent=self.agent,
            policy=self.agent.policy,
            global_step=self.ppo_vars.steps,
            metrics=metric_utils.MetricsGroup(self.train_metrics, 'train_metrics'))
        self.saved_model = policy_saver.PolicySaver(
            self.agent.policy, train_step=self.ppo_vars.steps)

        if config.runner.load_agent_and_policy:
            self.train_checkpointer.initialize_or_restore()

    # Abstract Methods
    def network_factory(self, shape: Sequence[int]) -> Tuple[ObservationPreprocessorBase, RewardPreprocessorBase]:
        raise NotImplementedError()

    def get_actor_distribution_net(self, train_env: TFEnvironment, config: NetConfig, preprocessor_model: Optional[tf.keras.Model]) -> DistributionNetwork:
        raise NotImplementedError()

    def get_value_network(self, train_env: TFEnvironment, config: NetConfig, preprocessor_model: Optional[tf.keras.Model]) -> Network:
        raise NotImplementedError()

    # Methods
    def make_initial_steps(self) -> None:
        print("make initial steps")
        random_policy = RandomTFPolicy(self.train_env.time_step_spec(), self.train_env.action_spec())

        max_len = 100

        replay_buffer = TFUniformReplayBuffer(
            data_spec=random_policy.collect_data_spec,
            batch_size=self.train_env.batch_size,
            max_length=max_len,
            )
        time_step: ts.TimeStep = self.train_env.reset()

        for i in range(self.config.runner.init_step_for_rnd):
            policy_step = random_policy.action(time_step)
            next_time_step = self.train_env.step(policy_step.action)
            trajectory = tf_agents.trajectories.trajectory.from_transition(time_step, policy_step, next_time_step)
            time_step = next_time_step
            replay_buffer.add_batch(trajectory)
            if i != 0 and i % max_len == 0:
                trajectories: Trajectory = replay_buffer.gather_all()
                self.train_env.fit(trajectories)
                replay_buffer.clear()

    def do_fixed_steps(self):
        a = np.array([1.0, 1.0], dtype=np.float32)
        policy_state = self.agent.policy.get_initial_state(self.eval_env.batch_size)

        fixed_policy = FixedPolicy(
            tf.constant(a, dtype=tf.float32),
            self.train_env.time_step_spec(),
            self.train_env.action_spec(),
            policy_info=policy_state)

        max_len = 100

        replay_buffer = TFUniformReplayBuffer(
            data_spec=fixed_policy.collect_data_spec,
            batch_size=self.train_env.batch_size,
            max_length=max_len)
        time_step: ts.TimeStep = self.train_env.reset()

        for i in range(self.config.runner.init_step_for_rnd):
            policy_step = fixed_policy.action(time_step)
            next_time_step = self.train_env.step(policy_step.action)
            trajectory = tf_agents.trajectories.trajectory.from_transition(time_step, policy_step, next_time_step)
            time_step = next_time_step
            replay_buffer.add_batch(trajectory)
            if i != 0 and i % max_len == 0:
                trajectories: Trajectory = replay_buffer.gather_all()
                self.agent.train(experience=trajectories)
                replay_buffer.clear()

    def get_preprocessor_model(self, config: PPOConfig, input_shape: Sequence[int]) -> Optional[tf.keras.models.Model]:
        if config.preprocessing_layers is None or len(config.preprocessing_layers) == 0:
            return None
        return tf.keras.models.Sequential([tf.keras.layers.Input(input_shape)] + config.preprocessing_layers)

    def get_agent(self, 
                    train_env: TFEnvironment,
                    optimizer: tf.compat.v1.train.Optimizer,
                    actor_distribution_net: DistributionNetwork,
                    value_network: Network,
                    ppo_config: PPOConfig) -> tf_agent:
        return PPOClipAgent(
                train_env.time_step_spec(),
                train_env.action_spec(),
                optimizer=optimizer,
                actor_net=actor_distribution_net,
                value_net=value_network,
                normalize_rewards=ppo_config.normalize_rewards,
                normalize_observations=ppo_config.normalize_observations,
                num_epochs=ppo_config.num_epochs,
                debug_summaries=ppo_config.debug_summary,
                summarize_grads_and_vars=ppo_config.summarize_grads_and_vars,
                lambda_value=ppo_config.lambda_value,
                discount_factor=ppo_config.discount_factor,
                importance_ratio_clipping=ppo_config.importance_ratio_clipping,
                name="PPOAgent"
            )

    def get_replay_buffer(self, agent: TFAgent, train_env: TFEnvironment, replay_buffer_max_length: int) -> ReplayBuffer:
        return TFUniformReplayBuffer(
            data_spec=agent.collect_data_spec,
            batch_size=train_env.batch_size,
            max_length=replay_buffer_max_length)

    def save_checkpoint(self, ppo_step_val: np.ndarray, save_policy: bool = True) -> None:
        if save_policy:
            self.train_checkpointer.save(global_step=ppo_step_val)
        self.train_env.save(self.saved_model_dir, ppo_step_val)
        self.ppo_vars.save(self.saved_model_dir, ppo_step_val)
        self.env_vars.save(self.saved_model_dir, ppo_step_val)
        self.eval_env_vars.save(self.saved_model_dir, ppo_step_val)

    def save_policy(self, ppo_step_val: np.ndarray) -> None:
        saved_model_path = os.path.join(
            str(self.saved_policy_model_dir), 'policy_' + ('%d' % ppo_step_val).zfill(9))
        self.saved_model.save(saved_model_path)

    def train_step(self) -> Tuple[tf.Tensor, tf.Tensor]:
        trajectories: Trajectory = self.replay_buffer.gather_all()
        self.train_env.fit(trajectories)
        return self.agent.train(experience=trajectories)

    def train_step_curiosity(self) -> None:
        trajectories: Trajectory = self.replay_buffer.gather_all()
        self.train_env.fit(trajectories)

    def eval(self, iteration: int) -> float:
        with tf.name_scope('eval'):
            with self.eval_summary_writer.as_default():
                # self.bug_reporter.reset()
                time_step: TimeStep = self.eval_env.reset()
                policy_state = self.agent.policy.get_initial_state(self.eval_env.batch_size) 
                for _ in range(self.config.runner.eval_steps):
                    policy_step = self.agent.policy.action(time_step, policy_state)
                    time_step = self.eval_env.step(policy_step.action)
                    policy_state = policy_step.state

                real_bugs_found, bugs_found = self.bug_reporter.eval()
                tf.summary.scalar('real_bugs_found', real_bugs_found, step=iteration)
                return real_bugs_found

    def train(self) -> None:
        collect_driver = DynamicStepDriver(
            self.train_env,
            self.agent.collect_policy,
            observers=[self.replay_buffer.add_batch] + self.train_metrics,
            num_steps=self.config.runner.collect_step_per_iteration)

        collect_time = 0.0
        train_time = 0.0
        time_at_step = self.ppo_vars.steps.numpy()

        self.train_env.reset()
        print("begin training")
        with self.ppo_vars.writer.as_default():
            iterations = 0
            while iterations < self.config.runner.num_iterations:
                ppo_step_val = self.ppo_vars.steps.numpy()
                env_step_val = self.env_vars.steps.numpy()
                start_time = time.time()

                collect_driver.run(maximum_iterations=self.config.runner.collect_step_per_iteration)
                collect_time += time.time() - start_time
                start_time = time.time()
                total_loss, _ = self.train_step()
                self.replay_buffer.clear()
                train_time += time.time() - start_time
                self.ppo_vars.steps = tf.add(self.ppo_vars.steps, 1)

                logging.info('iteration %d, loss = %f', iterations, total_loss)

                if iterations != 0 and iterations % self.config.runner.train_checkpoint_interval == 0:
                    logging.info('step = %d, loss = %f', env_step_val, total_loss)
                    steps_per_sec = (
                            (env_step_val - time_at_step) / (collect_time + train_time))
                    logging.info('%.3f steps/sec', steps_per_sec)
                    logging.info('collect_time = %.3f, train_time = %.3f', collect_time,
                                train_time)
                                
                    if self.config.runner.save_model:
                        self.save_checkpoint(ppo_step_val)

                    time_at_step = ppo_step_val
                    collect_time = 0
                    train_time = 0

                if iterations != 0 and iterations % self.config.runner.eval_interval == 0:
                    self.train_env.reset()
                    self.eval(iterations)

                if self.config.runner.save_model and iterations != 0 and iterations % self.config.runner.policy_checkpoint_interval == 0:
                    self.save_policy(ppo_step_val)

                iterations += 1

        ppo_step_val = self.ppo_vars.steps.numpy()
        self.save_checkpoint(ppo_step_val)
        self.save_policy(ppo_step_val)
        self.train_env.close()

    def run(self, n_steps: int) -> None:
        with tf.name_scope('run'):
            with self.eval_summary_writer.as_default():
                time_step: TimeStep = self.train_env.reset()
                policy_state = self.agent.policy.get_initial_state(self.train_env.batch_size) 
                for _ in range(n_steps):
                    policy_step = self.agent.policy.action(time_step, policy_state)
                    time_step = self.train_env.step(policy_step.action)
                    policy_state = policy_step.state

    def run_eval(self, n_steps: int) -> None:
        model_dir = OsPath(f"{datetime.now().strftime('%Y-%m-%d-%H-%M')}/")
        path = OsPath('eval/') + model_dir
        eval_env_vars = EnvStepVars(path + 'env_eval')
        eval_summary_writer = tf.summary.create_file_writer(str(path), flush_millis=1000, name="eval")
        self.eval_env = PlatformerTfEvalEnvironment(self.config.eval_env.env_name,
                                            OsPath(self.config.eval_env.data_folder),
                                            self.train_env.observation_preprocessor,
                                            self.train_env.reward_predictor,
                                            eval_env_vars,
                                            config=self.config.eval_env,
                                            bug_reporter=self.bug_reporter,
                                            debug_step_log_interval=self.config.eval_env.debug_step_log_interval,
                                            high_curiosity_threshold=self.config.eval_env.hight_curiosity_threshold)


        with tf.name_scope('eval'), eval_summary_writer.as_default():
            time_step: TimeStep = self.eval_env.reset()
            policy_state = self.agent.policy.get_initial_state(self.eval_env.batch_size)
            for _ in range(n_steps):
                policy_step = self.agent.policy.action(time_step, policy_state)
                time_step = self.eval_env.step(policy_step.action)
                policy_state = policy_step.state

    def train_curiosity_model(self) -> None:
        collect_driver = DynamicStepDriver(
            self.train_env,
            self.agent.collect_policy,
            observers=[self.replay_buffer.add_batch] + self.train_metrics,
            num_steps=self.config.runner.collect_step_per_iteration)

        collect_time = 0.0
        train_time = 0.0
        time_at_step = self.ppo_vars.steps.numpy()

        self.train_env.reset()
        print("begin training")
        with self.ppo_vars.writer.as_default():
            iterations = 0
            while iterations < self.config.runner.num_iterations:
                ppo_step_val = self.ppo_vars.steps.numpy()
                env_step_val = self.env_vars.steps.numpy()
                start_time = time.time()

                collect_driver.run(maximum_iterations=self.config.runner.collect_step_per_iteration)
                collect_time += time.time() - start_time
                start_time = time.time()
                self.train_step_curiosity()
                self.replay_buffer.clear()
                train_time += time.time() - start_time
                self.ppo_vars.steps = tf.add(self.ppo_vars.steps, 1)

                logging.info('iteration %d', iterations)

                if iterations % self.config.runner.train_checkpoint_interval == 0:
                    logging.info('step = %d', env_step_val)
                    steps_per_sec = ((env_step_val - time_at_step) / (collect_time + train_time))
                    logging.info('%.3f steps/sec', steps_per_sec)
                    logging.info('collect_time = %.3f, train_time = %.3f', collect_time,
                                 train_time)

                    if self.config.runner.save_model:
                        self.save_checkpoint(ppo_step_val, save_policy=False)

                    time_at_step = ppo_step_val
                    collect_time = 0
                    train_time = 0

                iterations += 1

        ppo_step_val = self.ppo_vars.steps.numpy()
        self.save_checkpoint(ppo_step_val, save_policy=False)
        self.save_policy(ppo_step_val)
        self.train_env.close()

    def _debug_env(self, env: PlatformerTfEnvironment) -> None:
        time_step: ts.TimeStep = self.train_env.reset()
        for _ in range(self.config.runner.num_iterations):
            action = self.agent.policy.action(time_step)
            time_step = env.step(action[0])

            observations: np.ndarray = time_step.observation.numpy()
            rewards: np.ndarray = time_step.reward
            batch_size = observations.shape[0]
            _, axs = plt.subplots(nrows=2, ncols=batch_size//2)
            axs_flatten = axs.flatten()

            for i in range(batch_size):
                obs = observations[i,:,:,:]
                reward = rewards[i]
                axs_flatten[i].imshow(obs)
                axs_flatten[i].set_title(f"instance {i}")
                print(f"instance {i}")
                print(f"reward {reward} and action {action[0][i]}")
            plt.show()
            print()

    def debug_train_env(self) -> None:
        self._debug_env(self.train_env)

    def debug_eval_env(self) -> None:
        self._debug_env(self.eval_env)
