from tensorboard import program

log_dir = "models\\2020-08-14-15-13"

if __name__ == '__main__':
    tb = program.TensorBoard()
    tb.configure(argv=[None, '--logdir', log_dir])
    url = tb.launch()
    print(url)
    input("press any key to exit")
