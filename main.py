import logging
import sys
import tensorflow as tf
from utlis.io_helper import OsPath
import os

from runners import BaseRunner
from runners.rnd_train_runner import RndTrainRunner
from runners.icm_train_runner import IcmTrainRunner
from runners.icm_rnn_train_runner import IcmRnnTrainRunner
from runners.rnd_rnn_train_runner import RndRnnTrainRunner
from runners.only_reward_runner import OnlyRewardRunner

# disable gpu
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
os.environ["CUDA_VISIBLE_DEVICES"] = "1"


def icm(config_path: OsPath) -> BaseRunner:
    # runner = IcmTrainRunner(config_path)
    runner = IcmRnnTrainRunner(config_path)
    return runner


def rnd(config_path: OsPath) -> BaseRunner:
    runner = RndRnnTrainRunner(config_path)
    # runner = RndTrainRunner(config_path)
    return runner


def only_intrinsic_reward(config_path: OsPath) -> BaseRunner:
    runner = OnlyRewardRunner(config_path)
    return runner


if __name__ == "__main__":
    logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
    # tf.compat.v1.enable_v2_behavior()

    # config_path = OsPath("./configs/only_reward.yaml")
    # config_path = OsPath("./configs/only_reward_icm.yaml")
    # config_path = OsPath("./configs/curiosity_small_extronsic_reward_icm.yaml")
    config_path = OsPath("./configs/rnd_curiosity_mixed.yaml")
    # config_path = OsPath("./configs/icm_curiosity_mixed.yaml")
    # config_path = OsPath("./configs/only_extrinsic_reward.yaml")

    # config_path = OsPath("./configs/rnd_curiosity_mixed_grayscale.yaml")
    # config_path = OsPath("./configs/icm_curiosity_mixed_grayscale.yaml")

    runner = rnd(config_path)
    runner.train()
    # runner.train_curiosity_model()
    # runner.run_eval(1000)

    # runner.run()
    # os.system("shutdown /s /t 1")

