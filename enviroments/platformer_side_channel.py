import logging
import uuid

import tensorflow as tf
from mlagents_envs.side_channel.side_channel import (
    SideChannel,
    IncomingMessage,
)


class PlatformerSideChannel(SideChannel):
    def __init__(self, batch_size: int) -> None:
        self.x = [0.0 for _ in range(batch_size)]
        self.y = [0.0 for _ in range(batch_size)]
        self.ix = [0 for _ in range(batch_size)]
        self.iy = [0 for _ in range(batch_size)]
        super(PlatformerSideChannel, self).__init__(uuid.UUID('14e56707-e8ad-4e25-b60b-ea1fb4c878f0'))

    def on_message_received(self, msg: IncomingMessage) -> None:
        instance_id = msg.read_int32()
        vector = msg.read_float32_list()
        if len(vector) != 2:
            logging.error(f"Received the dimension {vector} expected 2")
            return

        self.x[instance_id] = vector[0]
        self.y[instance_id] = vector[1]
        self.ix[instance_id] = msg.read_int32()
        self.iy[instance_id] = msg.read_int32()

    def log(self, instance_id: int) -> None:
        tf.summary.scalar("x", data=self.x[instance_id])
        tf.summary.scalar("y", data=self.y[instance_id])
        tf.summary.scalar("ix", data=self.ix[instance_id])
        tf.summary.scalar("iy", data=self.iy[instance_id])
