import contextlib
import threading
from typing import Iterator, Optional, Tuple, Sequence, Callable, List, Union, Any
from tf_agents.trajectories.trajectory import Trajectory
import collections
from collections import deque
import numpy as np
import tensorflow as tf
from mlagents_envs.side_channel.engine_configuration_channel import EngineConfigurationChannel
from tensorflow.python.autograph.impl import api as autograph
from tensorflow.python.framework import tensor_shape
from tf_agents.environments.tf_environment import TFEnvironment
from tf_agents.specs.tensor_spec import BoundedTensorSpec, TensorSpec
from tf_agents.trajectories import time_step as ts

from Networks.obervation_preprocessors import ObservationPreprocessorBase
from Networks.reward_preprocessor import RewardPreprocessorBase
from enviroments.platformer_side_channel import PlatformerSideChannel
from enviroments.unity_logger import UnityLogger
from my_unity_env import UnityEnv
from utlis.bug_reporter import BugReporter
from utlis.io_helper import OsPath
from utlis.explorable_area import ExplorableArea
from utlis.config_helper import EnvConfig

import sys
from utlis.runner_variables import EnvStepVars
import random

explorable_file_name = "explorable_area.yaml"

@contextlib.contextmanager
def _check_not_called_concurrently(lock: threading.Lock) -> Iterator:
    """Checks the returned context is not executed concurrently with any other."""
    if not lock.acquire(False):  # Non-blocking.
        raise RuntimeError(
            'Detected concurrent execution of TFPyEnvironment ops. Make sure the '
            'appropriate step_state is passed to step().')
    try:
        yield
    finally:
        lock.release()


class PlatformerTfEnvironment(TFEnvironment):
    worker_id = random.randint(0, 60530)

    def __init__(self,
                 unity_env_path: OsPath,
                 data_folder: OsPath,
                 network_factory: Optional[Callable[[Sequence[int]], Tuple[ObservationPreprocessorBase, RewardPreprocessorBase]]],
                 env_step_vars: EnvStepVars,
                 config: EnvConfig,
                 bug_reporter: BugReporter = None,
                 check_dims: bool = False,
                 debug_step_log_interval: int = 100,
                 high_curiosity_threshold: float = 0.4) -> None:

        self.config = config
        self.explorable_area = ExplorableArea(data_folder + 'explorable.json')
        self.high_curiosity_threshold = high_curiosity_threshold
        self.bug_reporter = bug_reporter
        self.vars = env_step_vars
        self.stacked_frames = config.stacked_frames

        self.global_writer = self.get_global_writer()
        self.agent_writers = []
        for i in range(config.batch_size):
            w_path = OsPath(str(self.vars.tf_writer_path) + f"_instance_{i}/")
            writer = tf.summary.create_file_writer(
                str(w_path), flush_millis=1000, name=f"env_instance_writer{i}")
            self.agent_writers.append(writer)

        channel = EngineConfigurationChannel()
        channel.set_configuration_parameters(time_scale=config.time_scale, width=config.preview_width, height=config.preview_height)

        self.platformer_side_channel = PlatformerSideChannel(config.batch_size)
        unity_logger = UnityLogger()

        self._u_env: UnityEnv = UnityEnv(str(unity_env_path),
                                         worker_id=PlatformerTfEnvironment.worker_id,
                                         use_visual=True,
                                         no_graphics=False,
                                         side_channels=[channel, self.platformer_side_channel, unity_logger],
                                         multiagent=config.batch_size > 1)
        PlatformerTfEnvironment.worker_id += 1

        action_spec: TensorSpec = BoundedTensorSpec(self._u_env.action_space.shape, dtype=np.float32,
                                              minimum=-1.5,
                                              maximum=1.5, name="action")

        self._time_step_dtypes = [tf.int32, tf.float32, tf.float32, tf.float32]
        self._time_step: Optional[ts.TimeStep] = None
        self._lock = threading.Lock()
        self._check_dims = check_dims
        self.last_img_report: tf.Variable = tf.Variable(tf.zeros(config.batch_size, tf.int64))
        self.debug_step_log_interval = debug_step_log_interval

        self._frames: deque = collections.deque(maxlen=self.stacked_frames)
        obs_shape = self._calc_obs_shape()

        self.observation_preprocessor: ObservationPreprocessorBase
        self.reward_predictor: RewardPreprocessorBase
        if network_factory is not None:
            self.observation_preprocessor, self.reward_predictor = network_factory(obs_shape)

        obs_out_shape = obs_shape
        observation_spec = BoundedTensorSpec(obs_out_shape,
                                                dtype=np.float32, minimum=0,
                                                maximum=1.0, name="observation")

        super().__init__(batch_size=config.batch_size,
                         action_spec=action_spec, 
                         time_step_spec=ts.time_step_spec(observation_spec))

        self.last_obs = tf.zeros(self.add_batch_size(obs_shape))
        self.last_actions = tf.zeros(self.add_batch_size(self._u_env.action_space.shape))

    def _calc_obs_shape(self) -> Sequence[int]:
        obs_shape = self._u_env.observation_space.shape
        # Update obs shape for stacked
        obs_shape_stacked = list(obs_shape)
        obs_shape_stacked[2] = obs_shape_stacked[2] * self.stacked_frames
        return obs_shape_stacked

    def _generate_observation(self) -> tf.Tensor:
        return tf.concat(list(self._frames), axis=-1)

    def get_global_writer(self) -> tf.summary.SummaryWriter:
        global_path = OsPath(str(self.vars.tf_writer_path) + '_global/')
        return tf.summary.create_file_writer(str(global_path), flush_millis=1000, name="env_global_writer")

    def load_explorable_area(self, path: OsPath) -> None:
        path = path.get_last_dir()
        if path is not None:
            file_path = path + explorable_file_name
            if file_path.exists():
                self.explorable_area.load(path + explorable_file_name)

    def load_predictor_model(self, path: OsPath) -> None:
        path = path.get_last_dir()
        if path is not None:
            self.reward_predictor.load(path)

    def save(self, path: OsPath, steps: np.ndarray) -> None:
        path = path + f"{int(steps)}/"
        self.explorable_area.save(path + f"{explorable_file_name}")
        self.reward_predictor.save(path)

    def add_batch_size(self, shape: Sequence[int]) -> Sequence[int]:
        return [self._batch_size] + list(shape)

    def _env_reset(self) -> np.ndarray:
        print("env reset")
        obs = self._u_env.reset()
        if self.batch_size == 1:
            o = np.array(obs)
            obs = o.reshape(self.add_batch_size(o.shape))
        else:
            obs = np.array(obs[0])
        return obs

    @autograph.do_not_convert()
    def _reset(self) -> ts.TimeStep:
        """Returns the current `TimeStep` after resetting the Environment."""
        def _reset_py() -> Union[List[np.ndarray], np.ndarray]:
            with _check_not_called_concurrently(self._lock):
                return self._env_reset()

        with tf.name_scope('reset'):
            obs = tf.numpy_function(
                _reset_py,
                [],  # No inputs.
                [tf.float32],
                name='reset_py_func')

            for _ in range(self.stacked_frames):
                self._frames.append(obs)
            self._time_step = ts.restart(self._generate_observation(), batch_size=self._batch_size)

            with tf.control_dependencies([obs]):
                return self._time_step

    @autograph.do_not_convert()
    def _current_time_step(self) -> ts.TimeStep:
        """Returns the current `TimeStep`."""

        if self._time_step is None:
            self._time_step = self._reset()

        return self._time_step

    def _env_step(self, actions: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """
          :returns
              observation (object/list): agent's observation of the current environment
              reward (float/list) : amount of reward returned after previous action
              done (boolean/list): whether the episode has ended.
              info (dict): contains auxiliary diagnostic information, including BatchedStepResult.
        """
        obs, reward, done, info = self._u_env.step(actions)

        if self.batch_size == 1:
            r = np.array(reward)
            o = np.array(obs)
            reward = r.reshape(self.add_batch_size(r.shape))
            obs = o.reshape(self.add_batch_size(o.shape))

        if done:
            return ts.termination(obs, reward=reward)
        else:
            return ts.transition(obs, reward=reward, discount=[1.0])

    def _step_py(self, *flattened_actions: Sequence[tf.Tensor]) -> tf.Tensor:
        with _check_not_called_concurrently(self._lock):
            packed = tf.nest.pack_sequence_as(
                structure=self.action_spec(), flat_sequence=flattened_actions)
            time_step = self._env_step(packed)
            return tf.nest.flatten(time_step)

    @autograph.do_not_convert()
    def _step(self, actions: tf.Tensor) -> ts.TimeStep:
        """Applies the action and returns the new `TimeStep`."""

        with tf.name_scope('step'), self.global_writer.as_default():
            self.vars.steps = self.vars.steps.assign_add(1, name='increment step')
            tf.summary.experimental.set_step(self.vars.steps)
            flat_actions = [tf.identity(x) for x in tf.nest.flatten(actions)]
            if self._check_dims:
                self._check_action_dims(flat_actions)

            outputs = tf.numpy_function(
                self._step_py,
                flat_actions,
                self._time_step_dtypes,
                name='step_py_func')
            step_type, reward, discount = outputs[0:3]
            reward = tf.clip_by_value(reward, -2, 2)

            # list [Tensor(4, 84, 84, 3)]
            flat_observations = outputs[3:]

            # Tensor(4, 84, 84, 3)
            x: tf.Tensor = flat_observations[0]
            if self.batch_size == 1:
                x_shape = self._u_env.observation_space.shape
                x = tf.reshape(x, [1] + list(x_shape))

            # frame stacking
            self._frames.append(x)
            x = self._generate_observation()

            curiosity = self.reward_predictor(x, last_obs=self.last_obs, current_actions=actions, last_actions=self.last_actions)
            self.last_actions = actions
            self.last_obs = x

            total_reward = reward * self.config.reward_factor + curiosity * self.config.curiosity_factor
            self._time_step = self._set_names_and_shapes(step_type, total_reward, discount, x)
            self._log_step(x, actions, curiosity, reward, total_reward)
            return self._time_step

    def fit(self, trajectories: Trajectory) -> None:
        traj_actions: tf.Tensor = trajectories.action
        traj_obs: tf.Tensor = trajectories.observation
        b = traj_actions.shape[0]
        s = traj_actions.shape[1]
        actions = tf.reshape(traj_actions, [b*s] + traj_actions.shape[2:])
        observations = tf.reshape(traj_obs, [b*s] + traj_obs.shape[2:])
        self.reward_predictor.fit(observations, actions)

    def _log_step(self, obs: tf.Tensor, actions: tf.Tensor, curiosity: tf.Tensor, reward: tf.Tensor, total_reward: tf.Tensor) -> None:
        if (self.vars.steps % self.debug_step_log_interval) == 0:
            for i in range(len(self.platformer_side_channel.ix)):
                self.explorable_area.register_explored(self.platformer_side_channel.ix[i], self.platformer_side_channel.iy[i])
            tf.summary.scalar('explored', data=self.explorable_area.eval())
            tf.summary.scalar('curiosity mean', data=tf.reduce_mean(curiosity))
            tf.summary.scalar('extrinsic reward mean', data=tf.reduce_mean(reward))
            tf.summary.scalar('total reward mean', data=tf.reduce_mean(total_reward))

            # Log for each instance
            with tf.name_scope('detailed'):
                for i in range(self.batch_size):
                    with self.agent_writers[i].as_default():
                        self.platformer_side_channel.log(i)
                        tf.summary.scalar(f'curiosity', data=curiosity[i])
                        tf.summary.scalar(f'extrinsic reward', data=reward[i])
                        tf.summary.scalar(f'total reward', data=total_reward[i])

                        for j, action in enumerate(actions[i]):
                            tf.summary.scalar(f'action_{j}', data=action)

                        if curiosity[i] > self.high_curiosity_threshold \
                                and tf.abs(self.vars.steps - self.last_img_report[i]) > 50:
                            print(f"hight curiosity in instance {i}: {curiosity[i]}")
                            self.last_img_report[i].assign(self.vars.steps)
                            tf.summary.image("high_curiosity_imgs", obs)
                            tf.summary.scalar("high_curiosity", 1)
                            if self.bug_reporter is not None:
                                self.bug_reporter.log_bug(self.platformer_side_channel.x[i], self.platformer_side_channel.y[i])
                        else:
                            tf.summary.scalar("high_curiosity", 0)

            if (self.vars.steps % 100) == 0:
                sys.stdout.write('\r')
                sys.stdout.write(f"step {self.vars.steps.numpy()} ")
                sys.stdout.write(f"curiosity {curiosity} ")
                sys.stdout.write(f"total reward {total_reward} ")
                sys.stdout.write(f"Explored {self.explorable_area.eval()} ")
                sys.stdout.write(f"actions {actions[0]} ")
                sys.stdout.flush()

    def _check_action_dims(self, flat_actions: List[tf.Tensor]) -> None:
        for action in flat_actions:
            dim_value = tensor_shape.dimension_value(action.shape[0])
            if (action.shape.rank == 0 or
                    (dim_value is not None and dim_value != self.batch_size)):
                raise ValueError(
                    'Expected actions whose major dimension is batch_size (%d), '
                    'but saw action with shape %s:\n   %s' %
                    (self.batch_size, action.shape, action))

    def render(self) -> None:
        pass

    def close(self) -> None:
        self._u_env.close()

    def _set_names_and_shapes(self, step_type: tf.Tensor, reward: tf.Tensor, discount: tf.Tensor, *flat_observations: tf.Tensor) -> ts.TimeStep:
        """Returns a `TimeStep` namedtuple."""
        step_type = tf.identity(step_type, name='step_type')
        reward = tf.identity(reward, name='reward')
        discount = tf.identity(discount, name='discount')
        batch_shape_tuple = () if not self.batched else (self.batch_size,)
        batch_shape = tf.TensorShape(batch_shape_tuple)
        if not tf.executing_eagerly():
            # Shapes are not required in eager mode.
            reward.set_shape(batch_shape)
            step_type.set_shape(batch_shape)
            discount.set_shape(batch_shape)
        # Give each tensor a meaningful name and set the static shape.
        named_observations = []
        for obs, spec in zip(flat_observations,
                             tf.nest.flatten(self.observation_spec())):
            named_observation = tf.identity(obs, name=spec.name)
            if not tf.executing_eagerly():
                named_observation.set_shape(batch_shape.concatenate(spec.shape))
            named_observations.append(named_observation)

        observations = tf.nest.pack_sequence_as(self.observation_spec(),
                                                named_observations)

        return ts.TimeStep(step_type, reward, discount, observations)