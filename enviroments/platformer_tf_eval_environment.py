from os import read
from typing import Sequence

from tensorflow_core.python import autograph
from tf_agents.trajectories.trajectory import Trajectory

from utlis.bug_reporter import BugReporter
from utlis.runner_variables import EnvStepVars
from Networks.reward_preprocessor import RewardPreprocessorBase
from Networks.obervation_preprocessors import ObservationPreprocessorBase
from utlis.io_helper import OsPath
from enviroments.platformer_tf_environment import PlatformerTfEnvironment
import tensorflow as tf
from tf_agents.trajectories import time_step as ts
from utlis.config_helper import EnvConfig


class PlatformerTfEvalEnvironment(PlatformerTfEnvironment):
    def __init__(self,
                 unity_env_path: OsPath,
                 data_folder: OsPath,
                 obs_preprocessor: ObservationPreprocessorBase,
                 reward_predictor: RewardPreprocessorBase,
                 env_step_vars: EnvStepVars,
                 bug_reporter: BugReporter,
                 config: EnvConfig,
                 check_dims: bool = False,
                 debug_step_log_interval: int = 1,
                 high_curiosity_threshold: float = 0.4) -> None:

        super(PlatformerTfEvalEnvironment, self).__init__(
            unity_env_path, 
            data_folder, 
            None,
            env_step_vars,
            config,
            bug_reporter, 
            check_dims,
            debug_step_log_interval,
            high_curiosity_threshold)

        self.observation_preprocessor = obs_preprocessor
        self.reward_predictor = reward_predictor
    
    def save(self, path: OsPath, steps: tf.Variable) -> None:
        raise NotImplementedError()
    
    def fit(self, trajectories: Trajectory) -> None:
        pass