from mlagents_envs.side_channel.side_channel import (
    SideChannel,
    IncomingMessage,
)
import uuid


class UnityLogger(SideChannel):
    def __init__(self) -> None:
        super(UnityLogger, self).__init__(uuid.UUID('ebd81812-4f51-46ea-af8f-e6fa1965640a'))

    def on_message_received(self, msg: IncomingMessage) -> None:
        message = msg.read_string()
        print(message)
