import wandb

wandb = wandb


def init_wandb() -> None:
    wandb.login(anonymous="never")
    wandb.init(project="automated-video-game-testing-mit-curiosity-driven-exploration", sync_tensorboard=True)