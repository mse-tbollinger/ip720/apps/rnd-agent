from typing import Tuple
from tensorflow import Tensor
import tensorflow as tf


class WelfordOnline:
    def __init__(self):
        self.count: tf.Variable = tf.Variable(0, dtype=tf.int64)
        self.mean: tf.Variable = tf.Variable(0.0, dtype=tf.float32)
        self.M2: tf.Variable = tf.Variable(0.0, dtype=tf.float32)

    def update(self, new_value: Tensor):
        self.count = self.count + 1
        delta: Tensor = new_value - self.mean
        self.mean = self.mean + delta / tf.cast(self.count, tf.float32)
        delta2: Tensor = new_value - self.mean
        self.M2 = self.M2 + delta * delta2

    def get_moments(self) -> Tuple[Tensor, Tensor, Tensor]:
        """
            :returns tuple (mean, variance, sample variance)
        """
        if self.count == 0:
            raise AssertionError("count == 0")
        else:
            return self.mean, self.M2 / tf.cast(self.count, tf.float32), self.M2 / tf.cast(self.count, tf.float32) - 1
