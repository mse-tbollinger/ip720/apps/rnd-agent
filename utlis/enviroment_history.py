from typing import Tuple
import tensorflow as tf


class EnvironmentHistory():
    def __init__(self, len: int) -> None:
        self.actions = tf.TensorArray(dtype=tf.float32, size=len, dynamic_size=False, clear_after_read=True)
        self.observations = tf.TensorArray(dtype=tf.float32, size=len, dynamic_size=False, clear_after_read=True)
        self._i = 0
        self.len = len

    def add(self, action: tf.Tensor, observation: tf.Tensor) -> None:
        if self._i >= self.len:
            raise IndexError("History max count reached")

        self.actions.write(self._i, action).mark_used()
        self.observations.write(self._i, observation).mark_used()
        self._i += 1

    def reset(self) -> None:
        self._i = 0

    def all_gathered(self) -> bool:
        return self._i == self.len - 1

    def gather(self) -> Tuple[tf.Tensor, tf.Tensor]:
        a = self.actions.stack()
        o = self.observations.stack()
        return tf.reshape(a, [-1] + list(a.shape[2:])), tf.reshape(o, [-1] + list(o.shape[2:]))
