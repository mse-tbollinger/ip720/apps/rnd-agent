from utlis.io_helper import OsPath
import pandas as pd
import numpy as np
from typing import Set, Tuple
import yaml


class ExplorableArea:
    def __init__(self, path: OsPath) -> None:
        df = pd.read_json(str(path))
        xs: np.ndarray = df['x'].to_numpy()
        ys: np.ndarray = df['y'].to_numpy()

        self.explorable: Set[Tuple[int, int]] = set()
        for (x, y) in zip(xs, ys):
            self.explorable.add((x, y))

        self.explored: Set[Tuple[int, int]] = set()

    def register_explored(self, x: int, y: int) -> None:
        vec = (x, y)
        if vec not in self.explored:
            self.explored.add(vec)

    def eval(self) -> float:
        diff = self.explorable.difference(self.explored)
        return 1 - (len(diff) / len(self.explorable))

    def load(self, path: OsPath) -> None:
        self.explored = yaml.load(path.open_read(), Loader=yaml.FullLoader)

    def save(self, path: OsPath) -> None:
        yaml.dump(self.explored, path.open_write())
