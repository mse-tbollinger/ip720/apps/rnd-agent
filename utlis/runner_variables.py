import tensorflow as tf
import yaml
import numpy as np

from utlis.io_helper import OsPath


class StepVarsBase:
    def __init__(self, steps: int, name: str):
        self.steps = tf.Variable(steps, dtype=tf.int64)
        self.name = name

    def save(self, path: OsPath, steps: np.ndarray):
        file = path + f"{int(steps)}/{self.name}.yaml"
        config = {'steps': int(self.steps.numpy())}
        yaml.dump(config, file.open_write())

    def load(self, path: OsPath):
        file = path.get_last_dir() + f"{self.name}.yaml"
        config = yaml.load(file.open_read(), Loader=yaml.FullLoader)
        self.steps = tf.Variable(config['steps'], dtype=tf.int64)


class EnvStepVars(StepVarsBase):
    def __init__(self, tf_writer_path: OsPath, steps: int = 0, name='env_step_vars'):
        super().__init__(steps, name)
        self.tf_writer_path = tf_writer_path


class PpoVars(StepVarsBase):
    def __init__(self, tf_writer: tf.summary.SummaryWriter, steps: int = 0, name='ppo_vars'):
        super().__init__(steps, name)
        self.writer = tf_writer
