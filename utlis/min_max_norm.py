import numpy as np
from tensorflow import Tensor
import tensorflow as tf


class MinMaxNorm:
    def __init__(self, remember_count: int):
        self.min = tf.Variable(np.inf)
        self.max = tf.Variable(-np.inf)
        self.max_i = -1
        self.min_i = -1
        self._remember_count = remember_count
        self._i = 0

        self.values = tf.TensorArray(tf.float32, size=remember_count, dynamic_size=False)

    def update(self, value: Tensor):
        with tf.name_scope("min_max_norm"):
            if self._i >= self._remember_count:
                self._i = 0

            if self._i == self.max_i:
                self.max_i = tf.argmax(self.values.stack())
                self.max = self.values.read(self.max_i)

            if self._i == self.min_i:
                self.min_i = tf.argmin(self.values.stack())
                self.min = self.values.read(self.min_i)

            self.values.write(self._i, value).mark_used()

            if value > self.max:
                self.max_i = self._i
                self.max = value
            if value < self.min:
                self.min_i = self._i
                self.min = value

            self._i += 1
            tf.summary.scalar('max', data=self.max)
            tf.summary.scalar('min', data=self.min)

    def normalize(self, x: Tensor):
        if self.min == self.max:
            return x
        return (x - self.min) / (self.max - self.min)
