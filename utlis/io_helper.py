from __future__ import annotations

import os
from typing import Union, TextIO, Optional


def join(a: str, b: str):
    a = os.path.normcase(a)

    if a[-1] != "/" and a[-1] != '\\':
        raise Exception(f"cannot join filename: {a} with filename {b}")

    if b[0] == "/" or b[0] == "\\":
        b = b[1:]

    return os.path.join(a, b)


class OsPath:
    def __init__(self, path: str):
        self._path = os.path.normcase(path)

    def __add__(self, other: Union[str, OsPath]):
        if isinstance(other, OsPath):
            return OsPath(join(self._path, other._path))
        else:
            return OsPath(join(self._path, other))

    def exists(self) -> bool:
        return os.path.exists(self._path)

    def open_read(self) -> TextIO:
        return open(self._path, "r")

    def open_write(self, options: str = '') -> TextIO:
        self._create_if_not_exists()
        return open(self._path, "w" + options)

    def _create_if_not_exists(self):
        directory = os.path.dirname(self._path)
        if not os.path.exists(directory):
            os.makedirs(directory)

    def get_last_dir(self) -> Optional[OsPath]:
        dirs = [dI for dI in os.listdir(self._path) if os.path.isdir(os.path.join(self._path, dI))]
        if len(dirs) > 0:
            steps = list(map(lambda x: int(x), dirs))
            steps.sort()
            step_dir = str(steps[-1])
            return self + f"/{step_dir}/"
        else:
            return None

    def __str__(self):
        self._create_if_not_exists()
        return self._path
