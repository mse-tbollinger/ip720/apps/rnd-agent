from __future__ import annotations
from typing import Dict, Any, List, Callable, Optional, Tuple

import yaml
from yamlinclude import YamlIncludeConstructor

from tensorflow.keras.layers import Layer
import tensorflow.keras.layers as layers
import tensorflow as tf

from Networks.normalizers import NormalizerBase
from Networks.normalizers.ClipNormalizer import ClipNormalizer
from Networks.normalizers.MaxNormalizer import MaxNormalizer
from Networks.normalizers.StdNormalizer import StdNormalizer
from Networks.normalizers.StreamingNormalizer import StreamingNormalizer
from Networks.normalizers.ZNormalzer import ZNormalizer
from Networks.normalizers.MinMaxNormalizer import MinMaxNormalizer
from Networks.normalizers.scale_normalizer import ScaleNormalizer
from utlis.io_helper import OsPath

YamlIncludeConstructor.add_to_loader_class(loader_class=yaml.SafeLoader, base_dir='configs/')


def serialize_layer(layer: Layer):
    layer_config = layer.get_config()
    config = {
        'class_name': type(layer).__name__,
        'config': layer_config
    }
    return config


def optional_key(dict: Dict[str, Any], key: str, other: Any = None) -> Any:
    if key in dict:
        return dict[key]
    return other


def deserialize_layer(layer_config: Dict[str, str]):
    return layers.deserialize(layer_config)


class ConfigBase:
    def __init__(self, config_dict: Dict[str, Any] = None):
        self.config_dict = config_dict

    def to_dict(self) -> Dict[str, Any]:
        raise NotImplementedError()

    def save(self, path: OsPath):
        yaml.dump(self.to_dict(), path.open_write(), Dumper=yaml.SafeDumper)


class PlatformerConfig(ConfigBase):
    def __init__(self, config_dict: Dict[str, Any] = None):
        super().__init__(config_dict)

        self.env: EnvConfig = EnvConfig()
        self.eval_env: EnvConfig = EnvConfig()
        self.runner: RunnerConfig = RunnerConfig()
        self.ppo: PPOConfig = PPOConfig()
        self.cnn_obs_preprocessor: CNNObsConfig = CNNObsConfig()
        self.rnd_reward_preprocessor: RNDRewardPreprocessorConfig = RNDRewardPreprocessorConfig()
        self.model_dir: Optional[str] = None

        if config_dict is not None:
            self.env = EnvConfig(config_dict['env']['train'])
            self.eval_env = EnvConfig(config_dict['env']['eval'])
            self.runner = RunnerConfig(config_dict['runner'])
            self.ppo = PPOConfig(config_dict['ppo'])
            self.cnn_obs_preprocessor = CNNObsConfig(config_dict['cnn_obs_preprocessor'])
            self.rnd_reward_preprocessor = RNDRewardPreprocessorConfig(config_dict['rnd_reward_preprocessor'])
            self.model_dir = config_dict['model_dir']

    def to_dict(self) -> Dict[str, Any]:
        return {
            'env': self.env.to_dict(),
            'runner': self.runner.to_dict(),
            'ppo': self.ppo.to_dict(),
            'cnn_obs_preprocessor': self.cnn_obs_preprocessor.to_dict(),
            'rnd_reward_preprocessor': self.rnd_reward_preprocessor.to_dict(),
            'model_dir': self.model_dir
        }

    @staticmethod
    def from_file(path: OsPath) -> PlatformerConfig:
        file_config = yaml.load(path.open_read(), Loader=yaml.SafeLoader)
        return PlatformerConfig(file_config)


class EnvConfig(ConfigBase):
    @property
    def env_name(self):
        return OsPath(self._env_name)

    @env_name.setter
    def env_name(self, path: OsPath):
        self._env_name = str(path)

    def __init__(self, config_dict: Dict[str, Any] = None):
        super().__init__(config_dict)

        self._env_name: str = "../../env/PlatformerSimple/Platformer Microgame.exe"
        self.preview_width: int = 800
        self.preview_height: int = 800
        self.time_scale: float = 800.0
        self.reward_factor: float = 2
        self.curiosity_factor: float = 1
        self.debug_step_log_interval: int = 100
        self.batch_size: int = 4
        self.data_folder: str = 'data/train/'
        self.hight_curiosity_threshold: float = 0.4
        self.stacked_frames: int = 4

        if config_dict is not None:
            self._env_name = config_dict['env_name']
            self.preview_width = optional_key(config_dict, 'preview_width', 800)
            self.preview_height = optional_key(config_dict, 'preview_height', 800)
            self.time_scale = float(config_dict['time_scale'])
            self.reward_factor = float(config_dict['reward_factor'])
            self.curiosity_factor = float(config_dict['curiosity_factor'])
            self.debug_step_log_interval = int(config_dict['debug_step_log_interval'])
            self.batch_size = int(config_dict['batch_size'])
            self.data_folder = str(config_dict['data_folder'])
            self.hight_curiosity_threshold = float(config_dict['hight_curiosity_threshold'])
            self.stacked_frames = int(config_dict['stacked_frames'])

    def to_dict(self) -> Dict[str, Any]:
        return {
            'env_name': self._env_name,
            'preview_width': self.preview_width,
            'preview_height': self.preview_height,
            'time_scale': self.time_scale,
            'reward_factor': self.reward_factor,
            'curiosity_factor': self.curiosity_factor,
            'debug_step_log_interval': self.debug_step_log_interval,
            'batch_size': self.batch_size,
            'data_folder': self.data_folder,
            'hight_curiosity_threshold': self.hight_curiosity_threshold,
        }

class RunnerConfig(ConfigBase):

    @property
    def model_root_dir(self) -> OsPath:
        return OsPath(self._model_root_dir)
    
    @model_root_dir.setter
    def model_root_dir(self, path: OsPath) -> None:
        self._model_root_dir = str(path)

    @property
    def model_dir(self) -> Optional[OsPath]:
        if self._model_dir is None:
            return None
        return OsPath(self._model_dir)

    @model_dir.setter
    def model_dir(self, path: OsPath) -> None:
        self._model_dir = str(path)

    def __init__(self, config_dict: Dict[str, Any] = None):
        super().__init__(config_dict)

        self.replay_buffer_max_length: int
        self.collect_step_per_iteration: int
        self.num_iterations: int
        self._model_root_dir: str
        self.train_checkpoint_interval: int
        self.policy_checkpoint_interval: int
        self.log_wandb: bool
        self.train: bool
        self.init_step_for_rnd: int
        self.load_agent_and_policy: bool
        self.load_reward_predictor: bool
        self.load_exploration: bool
        self.save_model: bool
        self.eval_steps: int
        self.eval_interval: int

        if config_dict is not None:
            self.replay_buffer_max_length = int(config_dict['replay_buffer_max_length'])
            self.collect_step_per_iteration = int(config_dict['collect_step_per_iteration'])
            self.num_iterations = int(config_dict['num_iterations'])
            self._model_root_dir = config_dict['model_root_dir']
            self.train_checkpoint_interval = int(config_dict['train_checkpoint_interval'])
            self.policy_checkpoint_interval = int(config_dict['policy_checkpoint_interval'])
            self.log_wandb = bool(optional_key(config_dict, 'log_wandb', True))
            self.train = bool(optional_key(config_dict, 'train', True))
            self.init_step_for_rnd = config_dict['init_step_for_rnd']
            self.load_agent_and_policy = bool(optional_key(config_dict, 'load_agent_and_policy', True))
            self.load_reward_predictor = bool(optional_key(config_dict, 'load_reward_predictor', True))
            self.load_exploration = bool(optional_key(config_dict, 'load_exploration', True))
            self.save_model = bool(optional_key(config_dict, 'save_model', True))
            self.eval_steps = int(optional_key(config_dict,'eval_steps', 1000))
            self.eval_interval = int(optional_key(config_dict,'eval_interval', 10))
        else:
            self.replay_buffer_max_length = 10_000
            self.collect_step_per_iteration = 1_000
            self.num_iterations = 5_000_000
            self._model_root_dir = "./models/"
            self.train_checkpoint_interval = 50
            self.policy_checkpoint_interval = 500
            self.log_wandb = True
            self.train = True
            self.init_step_for_rnd = 10000
            self.importance_ratio_clipping = 0.2
            self.load_agent_and_policy = True
            self.load_reward_predictor = True
            self.load_exploration = True
            self.save_model = True
            self.eval_steps = 1000
            self.eval_interval = 10

    def to_dict(self) -> Dict[str, Any]:
        return {
            'replay_buffer_max_length': self.replay_buffer_max_length,
            'collect_step_per_iteration': self.collect_step_per_iteration,
            'num_iterations': self.num_iterations,
            'model_root_dir': self._model_root_dir,
            'train_checkpoint_interval': self.train_checkpoint_interval,
            'policy_checkpoint_interval': self.policy_checkpoint_interval,
            'log_wandb': self.log_wandb,
            'train': self.train,
            'init_step_for_rnd': self.init_step_for_rnd.bit_length,
            'load_agent_and_policy': self.load_agent_and_policy,
            'load_reward_predictor': self.load_reward_predictor,
            'load_exploration':  self.load_exploration,
            'save_model': self.save_model,
            'eval_steps': self.eval_steps,
            'eval_interval': self.eval_interval,
        }


class PPOConfig(ConfigBase):

    @property
    def preprocessing_layers(self) -> Optional[List[Dict[str, Any]]]:
        return self._preprocessing_layers

    @preprocessing_layers.setter
    def preprocessing_layers(self, values: List[tf.keras.layers.Layer]) -> None:
        if values is not None:
            self._preprocessing_layers = values
            self._preprocessing_layers_config: Optional[List[Dict[str, Any]]] = [serialize_layer(layer) for layer in values]

    def __init__(self, config_dict: Dict[str, Any] = None):
        super().__init__(config_dict)
        self.num_epochs: int = 25
        self.normalize_rewards: bool = True
        self.normalize_observations: bool = True
        self.actor_distribution_net: NetConfig = NetConfig()
        self.value_network: NetConfig = NetConfig()
        self.learning_rate: float = 1e-3
        self.debug_summary: bool = True
        self.summarize_grads_and_vars: bool = True
        self.lambda_value: float = 0.95
        self.importance_ratio_clipping: float = 0.2
        self.discount_factor: float = 0.99
        self.normalize_observations = True
        self.use_gae: bool = True
        self.use_td_lambda_return: bool = True
        self._preprocessing_layers_config = None

        if config_dict is not None:
            self.num_epochs = int(config_dict['num_epochs'])
            self.normalize_rewards = bool(config_dict['normalize_rewards'])
            self.normalize_observations = bool(config_dict['normalize_observations'])
            self.actor_distribution_net = NetConfig(config_dict['actor_distribution_net'])
            self.value_network = NetConfig(config_dict['value_network'])
            self.learning_rate = optional_key(config_dict, 'learning_rate', 1e-3)
            self.debug_summary = optional_key(config_dict, 'debug_summary', True)
            self.summarize_grads_and_vars = optional_key(config_dict, 'summarize_grads_and_vars', True)
            self.lambda_value = optional_key(config_dict, 'lambda_value', 0.95)
            self.discount_factor = optional_key(config_dict, 'discount_factor', 0.99)
            self.importance_ratio_clipping = config_dict['importance_ratio_clipping']
            self.use_gae = config_dict['use_gae']
            self.use_td_lambda_return = config_dict['use_td_lambda_return']
            self._preprocessing_layers_config = optional_key(config_dict, 'preprocessing_layers', None)
            if self._preprocessing_layers_config is not None:
                self._preprocessing_layers = [deserialize_layer(dict) for dict in self._preprocessing_layers_config]
            else:
                self._preprocessing_layers = None

    def to_dict(self) -> Dict[str, Any]:
        return {
            'num_epochs': self.num_epochs,
            'normalize_rewards': self.normalize_rewards,
            'normalize_observations': self.normalize_observations,
            'actor_distribution_net': self.actor_distribution_net.to_dict(),
            'value_network': self.value_network.to_dict(),
            'learning_rate': self.learning_rate,
            'debug_summary': self.debug_summary,
            'summarize_grads_and_vars': self.summarize_grads_and_vars,
            'lambda_value': self.lambda_value,
            'discount_factor': self.discount_factor,
            'importance_ratio_clipping': self.importance_ratio_clipping,
            'use_gae': self.use_gae,
            'use_td_lambda_return': self.use_td_lambda_return,
            'preprocessing_layers': self._preprocessing_layers_config
        }


class NetConfig(ConfigBase):
    @property
    def activation_fn(self):
        return tf.keras.activations.deserialize(self._activation_fn)

    @activation_fn.setter
    def activation_fn(self, function: Callable[[], None]) -> None:
        self._activation_fn = function.__name__

    def __init__(self, config_dict: Dict[str, Any] = None):
        super().__init__(config_dict)

        self.fc_layer_params: List[int]
        self.conv_layer_params: List[Tuple[int, int, int]]
        self._preprocessing_layers_config: List[Dict[str, Any]]
        if config_dict is not None:
            self.fc_layer_params = config_dict['fc_layer_params']
            self._activation_fn = config_dict['activation_fn']
            self.conv_layer_params = optional_key(config_dict, 'conv_layer_params', None)
        else:
            self.fc_layer_params = [80, 80]
            self.conv_layer_params = [(64, 3, 1), (128, 3, 2)] # (filters, kernel_size, stride).
            self._activation_fn = 'relu'

    def to_dict(self) -> Dict[str, Any]:
        return {
            'fc_layer_params': self.fc_layer_params,
            'conv_layer_params' : self.conv_layer_params,
            'activation_fn': self._activation_fn
        }


class RNDRewardPreprocessorConfig(ConfigBase):
    def deserialize(self, config) -> Any:
        class_name = config['class_name']
        params = config['config']

        if class_name == 'MinMaxNormalizer':
            return MinMaxNormalizer(**params)
        elif class_name == 'ZNormalizer':
            return ZNormalizer(**params)
        elif class_name == "StreamingNormalizer":
            return StreamingNormalizer(**params)
        elif class_name == "ScaleNormalizer":
            return ScaleNormalizer(**params)
        elif class_name == "StdNormalizer":
            return StdNormalizer(**params)
        elif class_name == "MaxNormalizer":
            return MaxNormalizer(**params)
        elif class_name == "ClipNormalizer":
            return ClipNormalizer(**params)

    def __init__(self, config_dict: Dict[str, Any] = None):
        super().__init__(config_dict)

        self._curiosity_normalizer: List[Dict[str, Any]]
        self.curiosity_normalizer: List[NormalizerBase]
        self.dense_spec_target: List[int]
        self.dense_spec_predictor: List[int]

        if config_dict is not None:
            self._curiosity_normalizer: List[Dict[str, Any]] = config_dict['curiosity_normalizer']
            self.curiosity_normalizer: List[NormalizerBase] = [self.deserialize(dict) for dict in self._curiosity_normalizer]
            self.dense_spec_target:  List[int] = config_dict['dense_spec_target']
            self.dense_spec_predictor:  List[int] = config_dict['dense_spec_predictor']
        else:
            self._curiosity_normalizer: List[Dict[str, Any]] = [
                {
                    'class_name': 'ZNormalizer',
                    'config': {'input_shape': [0]}
                },
                {
                    'class_name': 'MinMaxNormalizer',
                    'config': {'input_shape': [0]}
                }
            ]
            self.curiosity_normalizer: List[NormalizerBase] = [ZNormalizer([1]), MinMaxNormalizer([1])]
            self.dense_spec_target: List[int] = [128, 264]
            self.dense_spec_predictor: List[int] = [128, 264]

    def to_dict(self) -> Dict[str, Any]:
        return {
            'curiosity_normalizer': self._curiosity_normalizer,
            'dense_spec_target': self.dense_spec_target,
            'dense_spec_predictor': self.dense_spec_predictor
        }


class CNNObsConfig(ConfigBase):
    @property
    def layers(self):
        return self._layers

    @layers.setter
    def layers(self, values: List[tf.keras.layers.Layer]):
        self._layers = values
        self._layers_config = [serialize_layer(layer) for layer in values]

    def __init__(self, config_dict: Dict[str, Any] = None):
        super().__init__(config_dict)
        self._layers_config: List[Dict[str, str]]
        self._layers: List[tf.keras.layers.Layer]

        if config_dict is not None:
            self._layers_config = config_dict['layers']
            self._layers = [deserialize_layer(dict) for dict in self._layers_config]
        else:
            self.layers = [
                tf.keras.layers.Conv2D(filters=32, kernel_size=3),
                tf.keras.layers.MaxPool2D(),
                tf.keras.layers.Conv2D(filters=64, kernel_size=3),
                tf.keras.layers.GlobalAveragePooling2D()
            ]

    def to_dict(self) -> Dict[str, Any]:
        return {
            'layers': self._layers_config
        }
