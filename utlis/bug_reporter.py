from utlis.io_helper import OsPath
import pandas as pd
from typing import List, Tuple
import numpy as np


class BugReporter:
    def __init__(self, true_bug_file: OsPath):
        self.df = pd.read_json(str(true_bug_file))
        self.bugs_recorded: List[np.ndarray] = []

    def log_bug(self, x: float, y: float) -> None:
        self.bugs_recorded.append(np.array([x, y]))

    def eval(self) -> Tuple[int, int]:
        '''
            returns:
                A Tuple with bugs found, real bugs found
        '''

        bugs_found: List[str] = []
        bugs_result: List[bool] = []

        for _, row in self.df.iterrows():
            found = False
            for predicted in self.bugs_recorded:
                bug = np.array([row['X'], row['Y']])
                if np.linalg.norm(bug - predicted) < row['Radius']:
                    bugs_found.append(row['name'])
                    found = True
            bugs_result.append(found)

        result = np.array(bugs_result)
        real_bugs_found = int(np.sum(bugs_result))
        print(f"real Bugs found {real_bugs_found} / {len(result)}")
        return real_bugs_found, len(bugs_found)

    def reset(self) -> None:
        self.bugs_recorded = []
