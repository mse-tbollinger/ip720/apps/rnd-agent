from tensorflow.keras.models import Model


def model_to_string(model: Model):
    string_list = []
    model.summary(print_fn=lambda x: string_list.append(x))
    model_summary = "\n".join(string_list)
    return model_summary
