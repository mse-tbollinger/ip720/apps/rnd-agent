from typing import Sequence, Dict
import tensorflow as tf
import yaml
from tensorflow import Tensor

from Networks.normalizers import NormalizerBase
from utlis.io_helper import OsPath


class ClipNormalizer(NormalizerBase):
    def __init__(self, input_shape: Sequence[int], min: float, max: float, name: str = "ClipNormalizer") -> None:
        self.min = min
        self.max = max
        super().__init__(input_shape, name)

    def load_params(self, path: OsPath) -> None:
        config = yaml.load(path.open_read())
        self.min = tf.Variable(config['min'])
        self.max = tf.Variable(config['max'])

    def __call__(self, x: Tensor, *args: Sequence[Tensor], **kwargs: Dict[str, Tensor]) -> Tensor:
        return tf.clip_by_value(x, self.min, self.max)

    def fit(self, x: Tensor) -> None:
        pass

    def save(self, path: OsPath) -> None:
        config = {
            'min': float(self.min),
            'max': float(self.max),
            }
        yaml.dump(config, path.open_write())

    def __str__(self) -> str:
        return f"ClipNormalizer"
