from typing import Sequence, Dict

import yaml
from Networks.normalizers import NormalizerBase
from utlis.io_helper import OsPath
from utlis.welford_online import WelfordOnline
import tensorflow as tf

from tensorflow import Tensor


class ZNormalizer(NormalizerBase):
    def __init__(self, input_shape: Sequence[int], decay: int = 0.01, model_path: OsPath = None, name="ZNormalizer"):
        super().__init__(input_shape, name)
        self.decay = decay
        self.welford = WelfordOnline()

    def load_params(self, path: OsPath):
        config = yaml.load(path.open_read())
        self.welford.count = tf.Variable(config['count'], dtype=tf.int64)
        self.welford.mean = tf.Variable(config['mean'], dtype=tf.float32)
        self.welford.M2 = tf.Variable(config['M2'], dtype=tf.float32)

    def __call__(self, x, *args: Sequence[Tensor], **kwargs: Dict[str, Tensor]):
        with tf.name_scope('z_normalizer'):
            self.welford.update(x)
            mean, var, _ = self.welford.get_moments()
            tf.summary.scalar('mean', mean[0])
            tf.summary.scalar('var', var[0])
            return (x - mean) / tf.square(var)

    def fit(self, x: Tensor) -> None:
        pass

    def save(self, path: OsPath):
        config = {
            'count': self.welford.count.numpy()[0],
            'mean': self.welford.mean.numpy()[0],
            'M2': self.welford.M2.numpy()[0]
        }
        yaml.dump(config, path.open_write())

    def __str__(self):
        return f"ZNormalizer(count: {self.welford.count}, " \
               f"mean: {self.welford.mean}, " \
               f"M2: {self.welford.M2}, " \
               f"decay: {self.decay})"
