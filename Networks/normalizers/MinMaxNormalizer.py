from typing import Dict, Sequence
import tensorflow as tf
import numpy as np

from Networks.normalizers import NormalizerBase
from utlis.io_helper import OsPath
import yaml


class MinMaxNormalizer(NormalizerBase):
    def __init__(self, input_shape: Sequence[int], decay: int = 0.01, warm_up_steps=0, name="MinMaxNormalizer") -> None:
        super().__init__(input_shape, name)
        self.min = tf.Variable(np.inf)
        self.max = tf.Variable(-np.inf)
        self.decay = decay
        self.warm_up_steps = warm_up_steps
        self.steps = 0

    def load_params(self, path: OsPath) -> None:
        if path.exists():
            config = yaml.load(path.open_read(), Loader=yaml.FullLoader)
            self.min = tf.Variable(config['min'])
            self.max = tf.Variable(config['max'])
            self.decay = config['decay']
            self.steps = config['steps']
            self.warm_up_steps = config['warm_up_steps']
            print(f"config loaded {path}")

    def normalize(self, x: tf.Tensor) -> tf.Tensor:
        if self.min == self.max:
            return x
        return (x - self.min) / (self.max - self.min)

    def __call__(self, x: tf.Tensor, *args: Sequence[tf.Tensor], **kwargs: Dict[str, tf.Tensor]) -> tf.Tensor:
        with tf.name_scope("min_max_norm"):
            self.steps += 1
            if self.steps > self.warm_up_steps:
                return self.normalize(x)
            else:
                return x

    def fit(self, x: tf.Tensor) -> None:
        if self.steps > self.warm_up_steps:
            if self.decay != 0:
                self.max = tf.minimum(self.max * (1 - self.decay), self.max * (1 + self.decay))
                self.min = tf.maximum(self.min * (1 - self.decay), self.min * (1 + self.decay))

            self.max = tf.maximum(self.max, tf.reduce_max(x))
            self.min = tf.minimum(self.min, tf.reduce_max(x))
            tf.summary.scalar('max', data=self.max)
            tf.summary.scalar('min', data=self.min)

    def save(self, path: OsPath) -> None:
        min: float
        max: float
        if self.steps <= self.warm_up_steps:
            min = float("inf")
            max = float("-inf")
        else:
            min = float(self.min.numpy())
            max = float(self.max.numpy())

        config = {
            'min': min,
            'max': max,
            'decay': self.decay,
            'steps': self.steps,
            'warm_up_steps': self.warm_up_steps
        }
        yaml.dump(config, path.open_write())

    def __str__(self) -> str:
        return f"MinMaxNormalizer(min: {self.min}, max: {self.max}, decay: {self.decay}, warmup steps: {self.warm_up_steps})"
