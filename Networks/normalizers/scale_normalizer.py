from typing import Sequence, Dict

import tensorflow as tf
from tensorflow import Tensor

from Networks.normalizers import NormalizerBase
from utlis.io_helper import OsPath


class ScaleNormalizer(NormalizerBase):
    def __init__(self, input_shape: Sequence[int], factor: float, name: str ="StreamingNormalizer") -> None:
        super().__init__(input_shape, name)
        self.factor = tf.constant(factor, dtype=tf.float32)

    def load_params(self, path: OsPath) -> None:
        pass

    def __call__(self, x: Tensor, *args: Sequence[Tensor], **kwargs: Dict[str, Tensor]) -> Tensor:
        with tf.name_scope(self.name):
            return tf.multiply(x, self.factor)

    def fit(self, x: Tensor) -> None:
        pass

    def plot_model(self, path: OsPath) -> None:
        pass

    def save(self, path: OsPath) -> None:
        pass

    def __str__(self) -> str:
        return f"ScaleNormalizer(factor: {self.factor})"
