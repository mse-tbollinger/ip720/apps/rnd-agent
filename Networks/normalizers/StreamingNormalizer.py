from typing import Sequence, Dict

import tensorflow as tf
import yaml
from tf_agents.utils.tensor_normalizer import StreamingTensorNormalizer
from Networks.normalizers import NormalizerBase
from utlis.io_helper import OsPath
from tensorflow import Tensor


class StreamingNormalizer(NormalizerBase):
    def __init__(self, input_shape: Sequence[int], clip_value: float = 5.0, center_mean: bool =True, name: str ="StreamingNormalizer") -> None:
        super().__init__(input_shape, name)
        self.clip_value = clip_value
        self.center_mean = center_mean
        self.streaming_tensor_normalizer = StreamingTensorNormalizer(tf.TensorSpec(input_shape))

    def load_params(self, path: OsPath) -> None:
        config = yaml.load(path.open_read())
        self.clip_value = config['clip_value']
        self.center_mean = config['center_mean']

    def __call__(self, x: Tensor, *args: Sequence[Tensor], **kwargs: Dict[str, Tensor]) -> Tensor:
        with tf.name_scope(self.name):
            return self.streaming_tensor_normalizer.normalize(x, self.clip_value, self.center_mean)

    def fit(self, x: Tensor) -> None:
        with tf.name_scope(self.name):
            self.streaming_tensor_normalizer.update(x)

    def save(self, path: OsPath) -> None:
        config = {
            'clip_value': self.clip_value,
            'center_mean': self.center_mean,
        }
        yaml.dump(config, path.open_write())

    def __str__(self) -> str:
        return f"StreamingNormalizer(clip_value: {self.clip_value}, " \
               f"center_mean: {self.center_mean})"
