from typing import Sequence, Dict

import tensorflow as tf
import tensorflow_probability as tfp
import yaml
from tensorflow import Tensor

from Networks.normalizers import NormalizerBase
from utlis.io_helper import OsPath


class StdNormalizer(NormalizerBase):
    def __init__(self, input_shape: Sequence[int], skip_steps=0, name: str = "StdNormalizer") -> None:
        super().__init__(input_shape, name)
        self.moving_mean = tf.Variable(0.0, dtype=tf.float32)
        self.moving_variance = tf.Variable(0.0, dtype=tf.float32)
        self.zero_debias_count = tf.Variable(0)
        self.i = 0
        self.std = tf.Variable(1.0)
        self.skip_steps = skip_steps

    def load_params(self, path: OsPath) -> None:
        config = yaml.load(path.open_read())
        self.moving_mean = tf.Variable(config['moving_mean'])
        self.moving_variance = tf.Variable(config['moving_variance'])
        self.zero_debias_count = tf.Variable(config['zero_debias_count'])
        self.i = config['i']
        self.std = tf.sqrt(self.moving_variance)

    def __call__(self, x: Tensor, *args: Sequence[Tensor], **kwargs: Dict[str, Tensor]) -> Tensor:
        self.i += 1
        if self.i <= self.skip_steps:
            return x
        else:
            return x / self.std

    def fit(self, x: Tensor) -> None:
        if self.i > self.skip_steps:
            with tf.name_scope(self.name):
                _, variance = tfp.stats.assign_moving_mean_variance(
                    value=x,
                    moving_mean=self.moving_mean,
                    moving_variance=self.moving_variance,
                    zero_debias_count=self.zero_debias_count,
                    decay=0.99,
                    axis=None)
            self.std = tf.sqrt(variance)
            tf.summary.scalar('std', self.std)

    def save(self, path: OsPath) -> None:
        config = {
            'moving_mean': float(self.moving_mean.numpy()),
            'moving_variance': float(self.moving_variance.numpy()),
            'zero_debias_count': float(self.zero_debias_count.numpy()),
            'i': self.i
            }
        yaml.dump(config, path.open_write())

    def __str__(self) -> str:
        return f"StdNormalizer"
