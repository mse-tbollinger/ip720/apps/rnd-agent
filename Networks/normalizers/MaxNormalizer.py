import yaml
from Networks.normalizers import NormalizerBase
from typing import Sequence, Dict
import numpy as np
import tensorflow as tf
from utlis.io_helper import OsPath


class MaxNormalizer(NormalizerBase):
    def __init__(self, input_shape: Sequence[int], decay: int = 0.01, warm_up_steps=0, name="MinMaxNormalizer") -> None:
        super().__init__(input_shape, name)
        self.max = tf.Variable(-np.inf)
        self.decay = decay
        self.warm_up_steps = warm_up_steps
        self.steps = 0

    def load_params(self, path: OsPath) -> None:
        if path.exists():
            config = yaml.load(path.open_read(), Loader=yaml.FullLoader)
            self.max = tf.Variable(config['max'])
            self.decay = config['decay']
            self.steps = config['steps']
            self.warm_up_steps = config['warm_up_steps']
            print(f"config loaded {path}")

    def normalize(self, x: tf.Tensor) -> tf.Tensor:
        if self.max == -np.inf:
            return x
        return x / self.max

    def __call__(self, x: tf.Tensor, *args: Sequence[tf.Tensor], **kwargs: Dict[str, tf.Tensor]) -> tf.Tensor:
        with tf.name_scope("max_norm"):
            self.steps += 1
            if self.steps > self.warm_up_steps:
                return self.normalize(x)
            else:
                return x

    def fit(self, x: tf.Tensor) -> None:
        with tf.name_scope("max_norm"):
            if self.steps > self.warm_up_steps:
                if self.decay != 0:
                    self.max = tf.minimum(self.max * (1 - self.decay), self.max * (1 + self.decay))

                self.max = tf.maximum(self.max, tf.reduce_max(x))
                tf.summary.scalar('max', data=self.max)

    def save(self, path: OsPath) -> None:
        max: float
        if self.steps <= self.warm_up_steps:
            max = float("-inf")
        else:
            max = float(self.max.numpy())

        config = {
            'max': max,
            'decay': self.decay,
            'steps': self.steps,
            'warm_up_steps': self.warm_up_steps
        }
        yaml.dump(config, path.open_write())

    def __str__(self) -> str:
        return f"MaxNormalizer(max: {self.max}, decay: {self.decay}, warmup steps: {self.warm_up_steps})"