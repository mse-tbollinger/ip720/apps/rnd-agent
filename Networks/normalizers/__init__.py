from collections import Sequence
from Networks import NetworkBase
from utlis.io_helper import OsPath
from typing import Dict, Tuple, Sequence
from tensorflow import Tensor


class NormalizerBase(NetworkBase):
    def __init__(self, input_shape: Sequence[int], name: str) -> None:
        super().__init__(input_shape)
        self.name = name

    @property
    def output_shape(self) -> Sequence[int]:
        return self.input_shape

    @property
    def params_file_name(self) -> str:
        return self.name + '.yaml'

    def __call__(self, x: Tensor, *args: Sequence[Tensor], **kwargs: Dict[str, Tensor]) -> Tensor:
        raise NotImplementedError()

    def fit(self, x: Tensor) -> None:
        raise NotImplemented()

    def load_params(self, path: OsPath) -> None:
        raise NotImplementedError()

    def save(self, path: OsPath) -> None:
        raise NotImplementedError()

    def __str__(self) -> str:
        raise NotImplementedError()
