from typing import Sequence, List, Any, Dict

import numpy as np
import tensorflow as tf
from tensorflow.keras import Model

import utlis.tf_utils as tf_utils
from Networks import NetworkBase
from Networks.obervation_preprocessors import ObservationPreprocessorBase
from Networks.obervation_preprocessors.clip_layer import ClipLayer
from utlis.io_helper import OsPath


# from https://gist.github.com/jkleint/eb6dc49c861a1c21b612b568dd188668
def shuffle_weights(model, weights=None):
    """Randomly permute the weights in `model`, or the given `weights`.
    This is a fast approximation of re-initializing the weights of a model.
    Assumes weights are distributed independently of the dimensions of the weight tensors
      (i.e., the weights have the same distribution along each dimension).
    :param Model model: Modify the weights of the given model.
    :param list(ndarray) weights: The model's weights will be replaced by a random permutation of these weights.
      If `None`, permute the model's current weights.
    """
    if weights is None:
        weights = model.get_weights()
    weights = [np.random.permutation(w.flat).reshape(w.shape) for w in weights]
    # Faster, but less random: only permutes along the first dimension
    # weights = [np.random.permutation(w) for w in weights]
    model.set_weights(weights)


class CnnObservationsPreprocessor(ObservationPreprocessorBase):
    def __init__(self,
                 input_shape: Sequence[int],
                 layers: List[tf.keras.layers.Layer] = None,
                 name: str = "cnn_observations_preprocessor"):

        super().__init__(input_shape, name)

        if layers is None:
            layers = [
                ClipLayer(-5, 5),
                tf.keras.layers.Conv2D(filters=32, kernel_size=3),
                tf.keras.layers.MaxPool2D(),
                tf.keras.layers.Conv2D(filters=64, kernel_size=3),
                tf.keras.layers.GlobalAveragePooling2D(),
                tf.keras.layers.Dense(32, tf.keras.activations.softmax)
            ]
        self.layers = layers

        self._model = tf.keras.models.Sequential(name=name)
        self._model.add(tf.keras.layers.Input(input_shape))
        self._model.add(ClipLayer(-5, 5))
        for layer in layers:
            self.model.add(layer)

        self.model.build([1] + list(input_shape))

    @property
    def model(self) -> Model:
        return self._model

    @property
    def output_shape(self) -> Sequence[int]:
        return self.model.output_shape

    def __call__(self, x):
        with tf.name_scope("cnn_obs_preprocessor"):
            return self.model(x)

    def __str__(self):
        return tf_utils.model_to_string(self.model)

    def clone(self) -> NetworkBase:
        cloned_model = tf.keras.models.clone_model(self._model)
        shuffle_weights(cloned_model)
        return CnnObservationsPreprocessor(self.input_shape, cloned_model.layers[1:], self.name + "_shallow_clone")

    def plot_model(self, path: OsPath) -> None:
        tf.keras.utils.plot_model(self.model, str(path + f"{self.name}.png"), show_shapes=True)

    def save(self, path: OsPath, *args: Sequence[Any], **kwargs: Dict[str, Any]) -> None:
        pass
