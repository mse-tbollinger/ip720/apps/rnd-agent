from __future__ import annotations
from abc import ABC
from typing import Sequence

from Networks import NetworkBase
from tensorflow.keras.models import Model


class ObservationPreprocessorBase(NetworkBase, ABC):
    def __init__(self,
                 input_shape: Sequence[int],
                 name: str = None):
        super().__init__(input_shape, name)

    @property
    def output_shape(self) -> Sequence[int]:
        raise NotImplementedError()

    @property
    def model(self) -> Model:
        raise NotImplementedError

    def __call__(self, *args, **kwargs):
        raise NotImplementedError()

    def __str__(self):
        raise NotImplementedError()

    def clone(self) -> ObservationPreprocessorBase:
        raise NotImplementedError()
