import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import Tensor


class ClipLayer(layers.Layer):
    def __init__(self, min_value: float, max_value: float):
        super(ClipLayer, self).__init__()

        self.min_value = min_value
        self.max_value = max_value

    def call(self, x: Tensor, **kwargs) -> Tensor:
        return tf.clip_by_value(x, self.min_value, self.max_value)

    def get_config(self):
        return {
            'min_value': self.min_value,
            'max_value': self.max_value,
        }
