from typing import Sequence, Any, Dict
from tensorflow.keras import Model
from tensorflow import Tensor

from Networks import NetworkBase
from Networks.obervation_preprocessors import ObservationPreprocessorBase
from utlis.io_helper import OsPath


class PassthroughObservationsPreprocessor(ObservationPreprocessorBase):
    def __init__(self,
                 input_shape: Sequence[int],
                 name: str = "passthrough_observations_preprocessor"):

        super().__init__(input_shape, name)
        self._model = Model()

    @property
    def model(self) -> Model:
        return self._model

    @property
    def output_shape(self) -> Sequence[int]:
        return self.input_shape
        
    def __call__(self, x: Tensor) -> Tensor:
        return x

    def __str__(self):
        return self.name

    def clone(self) -> NetworkBase:
        return self

    def plot_model(self, path: OsPath) -> None:
        pass

    def save(self, path: OsPath, *args: Sequence[Any], **kwargs: Dict[str, Any]) -> None:
        pass