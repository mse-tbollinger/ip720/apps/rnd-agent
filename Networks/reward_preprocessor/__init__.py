from typing import Dict, Sequence, Any
from utlis.io_helper import OsPath

from Networks import NetworkBase
from tensorflow import Tensor


class RewardPreprocessorBase(NetworkBase):
    def __init__(self,
                 input_shape: Sequence[int],
                 name: str = None):
        super().__init__(input_shape, name)

    @property
    def output_shape(self) -> Sequence[int]:
        raise NotImplementedError()

    def __call__(self, x: Tensor, *args: Sequence[Tensor], **kwargs: Dict[str, Tensor]) -> Tensor:
        raise NotImplementedError()

    def fit(self, observations: Tensor, actions: Tensor) -> None:
        raise NotImplementedError()

    def __str__(self) -> str:
        raise NotImplementedError()
    
    def plot_model(self, path: OsPath) -> None:
        ''' 
            path: folder to save the img
        ''' 
        raise NotImplementedError()

    def load(self, model_path: OsPath) -> None:
        raise NotImplementedError()

    def save(self, path: OsPath) -> None:
        raise NotImplementedError()
