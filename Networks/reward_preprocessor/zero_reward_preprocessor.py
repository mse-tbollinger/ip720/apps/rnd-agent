from typing import Dict, Sequence
import tensorflow as tf
from tensorflow import Tensor
from Networks.obervation_preprocessors import ObservationPreprocessorBase
from Networks.reward_preprocessor import RewardPreprocessorBase
from utlis.io_helper import OsPath


class ZeroRewardPreprocessor(RewardPreprocessorBase):

    def __init__(self,
                    obs_preprocessor: ObservationPreprocessorBase,
                    name: str = "rnd_reward_preprocessor") -> None:

        self.obs_preprocessor = obs_preprocessor
        super().__init__(obs_preprocessor.input_shape, name)

    def load(self, model_path: OsPath) -> None:
        pass

    @property
    def output_shape(self) -> Sequence[int]:
        return self.input_shape

    def __call__(self, x: Tensor, *args: Sequence[Tensor], **kwargs: Dict[str, Tensor]) -> Tensor:
        """
            :arg
                x: an Input Tensor in the shape of the obs_preprocessor.input_shape (agent_instances, w, h, c) (4, 96, 96, 3)
            :returns a Tensor in the shape of self.output_shape
        """
        batch_size = x.shape[0]
        return tf.zeros((batch_size))

    def fit(self, observations: Tensor, actions: Tensor) -> None:
        """
            observations: (batch_size * agent_instances, w, h, c)
            actions: (batch_size * agent_instances, action_len)
        """
        pass

    def __str__(self) -> str:
        return self.name

    def save(self, path: OsPath) -> None:
        pass

    def freeze(self) -> None:
        pass

    def plot_model(self, path: OsPath) -> None:
        pass
