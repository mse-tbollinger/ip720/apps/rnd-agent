from typing import Dict, Sequence, List
import tensorflow as tf
from tensorflow import Tensor
from tensorflow.keras.layers import Dense
import utlis.tf_utils as tf_utils
from Networks.normalizers import NormalizerBase
from Networks.obervation_preprocessors import ObservationPreprocessorBase
from Networks.reward_preprocessor import RewardPreprocessorBase
from utlis.io_helper import OsPath

pred_weights_name = "/predictor_model.tf"
target_weights_name = "/target_model.tf"


class RndRewardPreprocessor(RewardPreprocessorBase):

    def __init__(self,
                    obs_preprocessor: ObservationPreprocessorBase,
                    name: str = "rnd_reward_preprocessor",
                    dense_specs_target: List[int] = None,
                    dense_specs_predictor: List[int] = None,
                    curiosity_normalizer: List[NormalizerBase] = [],
                    optimizer=tf.optimizers.Adam()) -> None:

        if dense_specs_target is None:
            dense_specs_target = [128, 264]
        if dense_specs_predictor is None:
            dense_specs_predictor = [128, 264]

        self.preprocessor_model = obs_preprocessor
        self.curiosity_normalizer = curiosity_normalizer

        self.target_model = tf.keras.models.Sequential(name=name + "_target_model")
        obs_preprocessor_target: tf.keras.Model = self.preprocessor_model.clone().model
        for layer in obs_preprocessor_target.layers:
            self.target_model.add(layer)
            layer.trainable = False

        for layer in obs_preprocessor_target.layers:
            layer.trainable = False
        for spec in dense_specs_target[:-1]:
            self.target_model.add(Dense(spec, trainable=False, activation=tf.keras.activations.relu,
                                        kernel_initializer=tf.keras.initializers.RandomUniform))
        self.target_model.add(tf.keras.layers.Dense(dense_specs_target[-1], trainable=False,
                                                    kernel_initializer=tf.keras.initializers.RandomUniform))

        self.predictor_model = tf.keras.models.Sequential(name=name + "_predictor_model")

        for layer in self.preprocessor_model.model.layers:
            self.predictor_model.add(layer)
        for spec in dense_specs_predictor[:-1]:
            self.predictor_model.add(tf.keras.layers.Dense(spec, activation=tf.keras.activations.relu))
        self.predictor_model.add(tf.keras.layers.Dense(dense_specs_predictor[-1]))

        self.optimizer = optimizer
        super().__init__(self.preprocessor_model.input_shape, name)

        self.predictor_model.build([1] + list(self.input_shape))
        self.target_model.build([1] + list(self.input_shape))

        self.predictor_model.summary()
        self.target_model.summary()

    def load(self, model_path: OsPath) -> None:
        self.predictor_model.load_weights(str(model_path + pred_weights_name))
        self.target_model.load_weights(str(model_path + target_weights_name))
        for normalizer in self.curiosity_normalizer:
            normalizer.load_params(model_path + normalizer.params_file_name)

    @property
    def output_shape(self) -> Sequence[int]:
        return self.predictor_model.output_shape

    def __call__(self, x: Tensor, *args: Sequence[Tensor], **kwargs: Dict[str, Tensor]) -> Tensor:
        """
            :arg
                x: an Input Tensor in the shape of the obs_preprocessor.input_shape (agent_instances, w, h, c) (4, 96, 96, 3)
            :returns a Tensor in the shape of self.output_shape
        """
        with tf.name_scope("reward_preprocessor"):
            x = tf.image.per_image_standardization(x)
            pred_target = self.target_model(x)
            pred_predictor = self.predictor_model(x)
            loss = tf.keras.losses.MSE(pred_target, pred_predictor)

            x = loss
            for normalizer in self.curiosity_normalizer:
                x = normalizer(x)

            curiosity = x
            return curiosity

    def fit(self, observations: Tensor, actions: Tensor) -> None:
        """
            observations: (batch_size * agent_instances, w, h, c)
            actions: (batch_size * agent_instances, action_len)
        """
        with tf.name_scope("reward_preprocessor"):
            with tf.GradientTape() as tape:
                observations = tf.image.per_image_standardization(observations)
                pred_target = self.target_model(observations)
                pred_predictor = self.predictor_model(observations)
                loss = tf.losses.MSE(pred_target, pred_predictor)

                tf.summary.scalar("mean loss", tf.reduce_mean(loss))
                tf.summary.histogram("pred_target 0", pred_target)
                tf.summary.histogram("pred_predictor 0", pred_predictor)

                for w in self.target_model.weights:
                    w: tf.Variable
                    tf.summary.histogram("target_model_" + w.name, w)

                for w in self.predictor_model.weights:
                    w: tf.Variable
                    tf.summary.histogram("predictor_model_" + w.name, w)

            if not self.is_frozen:
                gradients = tape.gradient(loss, self.predictor_model.trainable_variables)
                self.optimizer.apply_gradients(zip(gradients, self.predictor_model.trainable_variables))

                x = loss
                for normalizer in self.curiosity_normalizer:
                    normalizer.fit(x)
                    x = normalizer(x)

    def __str__(self) -> str:
        return self.name + "\n" + \
            str(self.preprocessor_model) + "\n" +\
            tf_utils.model_to_string(self.target_model) + "\n" +\
            tf_utils.model_to_string(self.predictor_model)

    def save(self, path: OsPath) -> None:
        self.predictor_model.save_weights(str(path + pred_weights_name))
        self.target_model.save_weights(str(path + target_weights_name))

        for normalizer in self.curiosity_normalizer:
            normalizer.save(path + normalizer.params_file_name)

    def freeze(self) -> None:
        super().freeze()
        for layer in self.predictor_model.layers:
            layer.trainable = False

    def plot_model(self, path: OsPath) -> None:
        tf.keras.utils.plot_model(self.predictor_model, str(path + "predictor_model.png"), show_shapes=True, expand_nested=True)
        tf.keras.utils.plot_model(self.target_model, str(path + "target_model.png"), show_shapes=True, expand_nested=True)
