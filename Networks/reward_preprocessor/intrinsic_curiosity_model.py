from Networks.obervation_preprocessors.clip_layer import ClipLayer
from os import name
from typing import Dict, Optional, Sequence, List
import tensorflow as tf
from tensorflow import Tensor
from tensorflow.keras.layers import Dense, Input, Concatenate
import utlis.tf_utils as tf_utils
from Networks.normalizers import NormalizerBase
from Networks.obervation_preprocessors import ObservationPreprocessorBase
from Networks.reward_preprocessor import RewardPreprocessorBase
from utlis.io_helper import OsPath
from Networks.debug.print_layer import Print

forward_weights_name = "/forward_model.tf"
invers_weights_name = "/inverse_model.tf"
icm_weights_name = "/icm_model.tf"


class IntrinsicCuriosityModel(RewardPreprocessorBase):
    def __init__(self,
                 obs_preprocessor: ObservationPreprocessorBase,
                 action_len: int,
                 name: str = "rnd_reward_preprocessor",
                 dense_specs_forward_model: Optional[List[int]] = None,
                 dense_specs_invers_model: Optional[List[int]] = None,
                 curiosity_normalizer: List[NormalizerBase] = [],
                 optimizer: tf.optimizers.Optimizer = tf.optimizers.Adam()):

        if dense_specs_forward_model is None:
            dense_specs_forward_model = [128, 264]
        if dense_specs_invers_model is None:
            dense_specs_invers_model = [128, 264]

        self.preprocessor_model = obs_preprocessor
        self.curiosity_normalizer = curiosity_normalizer

        self.forward_model = self.create_forward_model(dense_specs_forward_model, action_len)
        self.inverse_model = self.create_inverse_model(dense_specs_invers_model, action_len)
        self.icm_model = self.create_icm_model(action_len, name)

        self.optimizer = optimizer

        super().__init__(self.preprocessor_model.input_shape, name)

        self.inverse_model.build([1] + list(self.input_shape))
        self.forward_model.build([1] + list(self.input_shape))

    def load(self, model_path: OsPath) -> None:
        self.inverse_model.load_weights(str(model_path + icm_weights_name))
        for normalizer in self.curiosity_normalizer:
            normalizer.load_params(model_path + normalizer.params_file_name)

    @property
    def output_shape(self) -> Sequence[int]:
        return self.inverse_model.output_shape

    def create_forward_model(self, dense_specs_forward_model: List[int], action_len: int) -> tf.keras.Model:
        forward_model = tf.keras.models.Sequential(name=name + "_forward_model")
        feature_spec = self.preprocessor_model.output_shape[1]
        forward_model.add(Input(shape=feature_spec + action_len))

        for spec in dense_specs_forward_model:
            forward_model.add(Dense(spec, activation=tf.keras.activations.relu))
        forward_model.add(tf.keras.layers.Dense(feature_spec, activation=tf.keras.activations.relu))
        return forward_model

    def create_inverse_model(self, dense_specs_invers_model: List[int], action_len: int) -> tf.keras.Model:
        inverse_model = tf.keras.models.Sequential(name=name + "_inverse_model")
        inverse_model.add(Input(shape=self.preprocessor_model.output_shape[1] * 2))
        for spec in dense_specs_invers_model:
            inverse_model.add(tf.keras.layers.Dense(spec, activation=tf.keras.activations.relu))
        inverse_model.add(tf.keras.layers.Dense(action_len, activation=tf.keras.activations.tanh))
        inverse_model.add(ClipLayer(-0.9, 0.9))
        return inverse_model

    def create_icm_model(self, action_len: int, name: str) -> tf.keras.Model:
        obs_t0 = Input(shape=self.preprocessor_model.input_shape, name="obs_t0")
        obs_t1 = Input(shape=self.preprocessor_model.input_shape, name="obs_t1")
        feature_t0 = self.preprocessor_model(obs_t0)
        feature_t1 = self.preprocessor_model(obs_t1)

        action_t0 = Input(shape=[action_len], name="action_t0")
        action_t1 = Input(shape=[action_len], name="action_t1")
        action_t0_normalized = ClipLayer(-0.9, 0.9)(action_t0)
        action_t1_normalized = ClipLayer(-0.9, 0.9)(action_t1)

        action_t0_normalized_debug = Print("action_t0_normalized: ")(action_t0_normalized)
        action_t1_normalized_debug = Print("action_t1_normalized: ")(action_t1_normalized)

        forward_input = Concatenate(name="forward_input")([feature_t0, action_t0_normalized_debug])
        pred_feature_t1 = self.forward_model(forward_input)

        invers_input = Concatenate(name="invers_input")([feature_t0, feature_t1])
        pred_action = self.inverse_model(invers_input)

        pred_action_debug = Print("pred_action: ")(pred_action)

        inverse_loss = tf.keras.losses.MSE(pred_action_debug, action_t1_normalized_debug)
        inverse_loss_debug = Print("inverse_loss_debug: ")(inverse_loss)
        forward_loss = tf.keras.losses.MSE(pred_feature_t1, feature_t1)
        icm_loss = 0.2 * inverse_loss_debug + 0.8 * forward_loss
        return tf.keras.Model(inputs=[obs_t0, obs_t1, action_t0, action_t1], outputs=[forward_loss, inverse_loss, icm_loss],
                              name=name)

    def __call__(self, x: Tensor, *args: Sequence[Tensor], **kwargs: Dict[str, Tensor]) -> Tensor:
        """
            :arg
                x: current observations:  an Input Tensor in the shape of the obs_preprocessor.input_shape (agent_instance, w, h, c)
                last_obs: the previous observations: an Input Tensor in the shape of the obs_preprocessor.input_shape (agent_instance, w, h, c)
                current_actions: an Input Tensor with the environment actions (agent_instance, action_len)
                last_actions: an Input Tensor with the environment actions (agent_instance, action_len)
            :returns a Tensor in the shape of self.output_shape
        """
        if 'last_obs' not in kwargs:
            raise ValueError("last_obs kkwargs is not set")
        if 'current_actions' not in kwargs:
            raise ValueError("current_actions kkwargs is not set")
        if 'last_actions' not in kwargs:
            raise ValueError("last_actions kkwargs is not set")

        obs_t0: Tensor = kwargs['last_obs']
        obs_t1 = x
        action_t0: Tensor = kwargs['last_actions']
        action_t1: Tensor = kwargs['current_actions']

        obs_t0 = tf.image.per_image_standardization(obs_t0)
        obs_t1 = tf.image.per_image_standardization(obs_t1)

        with tf.name_scope("icm"):
            reward, _, _ = self.icm_model([obs_t0, obs_t1, action_t0, action_t1])

            x = reward
            for normalizer in self.curiosity_normalizer:
                x = normalizer(x)

            curiosity = x
            return curiosity

    def fit(self, observations: Tensor, actions: Tensor) -> None:
        """
            :arg
                observations: history of observations (batch_size * agent_instances, w, h, c)
                actions: history of actions (batch_size * agent_instances, action_len)
            :returns a Tensor in the shape of self.output_shape
        """
        with tf.name_scope("icm"):
            obs_t0 = tf.slice(observations, [0, 0, 0, 0], [observations.shape[0] - 1] + observations.shape[1:])
            obs_t1 = tf.slice(observations, [1, 0, 0, 0], [observations.shape[0] - 1] + observations.shape[1:])
            action_t0 = tf.slice(actions, [0, 0], [actions.shape[0] - 1] + actions.shape[1:])
            action_t1 = tf.slice(actions, [1, 0], [actions.shape[0] - 1] + actions.shape[1:])

            obs_t0 = tf.image.per_image_standardization(obs_t0)
            obs_t1 = tf.image.per_image_standardization(obs_t1)

            with tf.GradientTape() as tape:
                forward_loss, inverse_loss, loss = self.icm_model([obs_t0, obs_t1, action_t0, action_t1])
                tf.summary.scalar("mean loss", tf.reduce_mean(loss))
                tf.summary.scalar("forward_loss_mean", tf.reduce_mean(forward_loss))
                tf.summary.scalar("inverse_loss_mean", tf.reduce_mean(inverse_loss))
            if not self.is_frozen:
                gradients = tape.gradient(loss, self.icm_model.trainable_variables)
                self.optimizer.apply_gradients(zip(gradients, self.icm_model.trainable_variables))

            x = forward_loss
            for normalizer in self.curiosity_normalizer:
                normalizer.fit(x)
                x = normalizer(x)

    def __str__(self) -> str:
        return self.name + "\n" + \
               str(self.preprocessor_model) + "\n" + \
               tf_utils.model_to_string(self.forward_model) + "\n" + \
               tf_utils.model_to_string(self.inverse_model) + "\n" + \
               tf_utils.model_to_string(self.icm_model)

    def save(self, path: OsPath) -> None:
        self.icm_model.save_weights(str(path + icm_weights_name))

        for normalizer in self.curiosity_normalizer:
            normalizer.save(path + normalizer.params_file_name)

    def freeze(self) -> None:
        super().freeze()
        for layer in self.forward_model.layers:
            layer.trainable = False

        for layer in self.inverse_model.layers:
            layer.trainable = False

    def plot_model(self, path: OsPath) -> None:
        tf.keras.utils.plot_model(self.icm_model, str(path + "icm_model.png"), show_shapes=True)
        tf.keras.utils.plot_model(self.forward_model, str(path + "forward_model.png"), show_shapes=True)
        tf.keras.utils.plot_model(self.inverse_model, str(path + "invers_model.png"), show_shapes=True)