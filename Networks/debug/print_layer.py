from tensorflow.keras.layers import Layer
import tensorflow as tf

class Print(Layer):
    verbose=True

    def __init__(self, message="", fn=lambda arg: arg, **kwargs) -> None:
        super(Print, self).__init__(**kwargs)
        self.message=message
        self.fn=fn
        
    def get_config(self):
        return super(Print, self).get_config()

    def call(self, inputs, *args, **kwargs):
        if Print.verbose:
            # tf.print(self.message, self.fn(inputs))
            tf.summary.histogram(self.message, inputs)
        return super(Print, self).call(inputs, *args, **kwargs)
      
    @classmethod
    def print(cls,*args, **kwargs):    
        if Print.verbose:
            tf.print(*args, **kwargs)