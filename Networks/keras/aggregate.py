import tensorflow as tf
from tensorflow.keras import layers
from tensorflow import Tensor


class Aggregate(layers.Layer):
    def __init__(self) -> None:
        super(Aggregate, self).__init__()

    def call(self, x: Tensor, **kwargs) -> Tensor:
        sum = tf.reduce_sum(x, axis=1)
        return tf.reshape(sum, [1])
