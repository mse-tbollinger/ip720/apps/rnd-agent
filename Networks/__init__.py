from typing import Dict, List, Sequence, Any

from utlis.io_helper import OsPath
from tensorflow import Tensor


class NetworkBase:
    def __init__(self,
                 input_shape: Sequence[int],
                 name: str = None):

        self.input_shape = input_shape
        if name is None:
            name = ""
        self.name = name
        self.is_frozen = False

    @property
    def output_shape(self) -> Sequence[int]:
        raise NotImplementedError()

    def __call__(self, x: Tensor, *args: Sequence[Tensor], **kwargs: Dict[str, Tensor]) -> Tensor:
        raise NotImplementedError()

    def save(self, path: OsPath, *args: Sequence[Any], **kwargs: Dict[str, Any]) -> None:
        raise NotImplementedError()

    def __str__(self) -> str:
        raise NotImplementedError()

    def freeze(self) -> None:
        self.is_frozen = True

    def plot_model(self, path: OsPath) -> None:
        ''' 
            path: folder to save the img
        ''' 
        raise NotImplementedError()